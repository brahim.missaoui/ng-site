package com.kapsulstore.ngsite.config;

import com.kapsulstore.core.service.search.KSDbSearchService;
import com.kapsulstore.ngsite.catalog.dao.KSProductOptionDaoImpl;
import com.kapsulstore.ngsite.web.rest.catalog.KSCategoriesWrapper;
import com.kapsulstore.ngsite.web.rest.checkout.KSPaymentGatewayCheckoutService;
import io.github.jhipster.config.JHipsterConstants;
import io.github.jhipster.config.liquibase.AsyncSpringLiquibase;
import liquibase.integration.spring.SpringLiquibase;
import org.broadleafcommerce.common.extensibility.jpa.MergePersistenceUnitManager;
import org.broadleafcommerce.common.payment.service.PaymentGatewayCheckoutService;
import org.broadleafcommerce.core.catalog.dao.ProductOptionDao;
import org.broadleafcommerce.core.search.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by mehmet on 5/6/17.
 */
@Configuration
@EnableJpaRepositories("com.kapsulstore.ngsite.repository")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class BroeadleafDbMergeConfig {


    private final Logger log = LoggerFactory.getLogger(BroeadleafDbMergeConfig.class);

    private final Environment env;

    public BroeadleafDbMergeConfig(Environment env) {
        this.env = env;
    }

    @Bean("blSearchService")
    @Primary
    public SearchService blSearchService() {
        return new KSDbSearchService();
    }

    @Bean("blProductOptionDao")
    @Primary
    public ProductOptionDao blProductOptionDao() {
        return new KSProductOptionDaoImpl();
    }

    @Bean("blPaymentGatewayCheckoutService")
    @Primary
    public PaymentGatewayCheckoutService ksPaymentGatewayCheckoutService(){
        return new KSPaymentGatewayCheckoutService();
    }

/*

    @Bean("entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean myPU(@Qualifier("blPersistenceUnitManager") MergePersistenceUnitManager  blMergePersistenceUnitManager){
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setPersistenceUnitManager(blMergePersistenceUnitManager);
        entityManagerFactoryBean.setPersistenceUnitName("blPU");
        entityManagerFactoryBean.setPackagesToScan("com.kapsulstore.ngsite.domain");
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        return entityManagerFactoryBean;
    }
*/

   /* @Bean
    public SpringLiquibase liquibase(@Qualifier("taskExecutor") TaskExecutor taskExecutor,
                                     @Qualifier("webDS") DataSource dataSource, LiquibaseProperties liquibaseProperties) {

        // Use liquibase.integration.spring.SpringLiquibase if you don't want Liquibase to start asynchronously
        SpringLiquibase liquibase = new AsyncSpringLiquibase(taskExecutor, env);
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:config/liquibase/master.xml");
        liquibase.setContexts(liquibaseProperties.getContexts());
        liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
        liquibase.setDropFirst(liquibaseProperties.isDropFirst());
        if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_NO_LIQUIBASE)) {
            liquibase.setShouldRun(false);
        } else {
            liquibase.setShouldRun(liquibaseProperties.isEnabled());
            log.debug("Configuring Liquibase");
        }
        return liquibase;
    }*/

}
