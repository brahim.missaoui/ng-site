package com.kapsulstore.ngsite.web.rest.checkout;

import com.broadleafcommerce.rest.api.wrapper.OrderWrapper;
import com.iyzipay.Options;
import com.iyzipay.model.*;
import com.iyzipay.request.CreateCheckoutFormInitializeRequest;
import com.iyzipay.request.CreatePaymentRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mehmet on 7/11/17.
 */
public class IyzicoBuilder {



    private static com.iyzipay.Options getTestOptions(){
        final String API_KEY = "sandbox-gEYoXRnKj8O9oukmt5cxEm4EpsZ4a57j";
        final String SECRET_KEY = "sandbox-4IXtAqW3kPFdpnvcIxSsMCI2h7swmP7T";
        final String BASE_URL = "https://sandbox-api.iyzipay.com";
        com.iyzipay.Options retVal = new Options();
        retVal.setApiKey(API_KEY);
        retVal.setSecretKey(SECRET_KEY);
        retVal.setBaseUrl(BASE_URL);
        return retVal;
    }
    public static com.iyzipay.Options getOptions(){
        //return IyzicoBuilder.getTestOptions();
        return IyzicoBuilder.getProdOptions();
    }
    public static String callBackHostname(){

        //return "http://local.kapsulstore.com:9001"; //test
        return "https://www.kapsulstore.com"; //prod
    }

    private static com.iyzipay.Options getProdOptions(){
        final String API_KEY = "fLgmNvywW1VTZ6DSU9aNOzkKD1bQ8lDa";
        final String SECRET_KEY = "AN8VxHBk9SvHl1fZTUQlosyU6aXy8fTA";
        final String BASE_URL = "https://api.iyzipay.com";
        com.iyzipay.Options retVal = new Options();
        retVal.setApiKey(API_KEY);
        retVal.setSecretKey(SECRET_KEY);
        retVal.setBaseUrl(BASE_URL);
        return retVal;
    }

    public static Map<String,String> paymentFormResultToMap(CheckoutFormInitialize checkoutFormInitialize){
        Map<String,String> map = new HashMap<>();
        map.put("IYZI_FORM_INIT_RESULT_STATUS", checkoutFormInitialize.getStatus());
        map.put("IYZI_FORM_INIT_RESULT_PAYMENTURL", checkoutFormInitialize.getPaymentPageUrl());
        map.put("IYZI_FORM_INIT_RESULT_CHECKOUT_FORM_CONTENT", checkoutFormInitialize.getCheckoutFormContent());
        map.put("IYZI_FORM_INIT_RESULT_CONVERSATION_ID", checkoutFormInitialize.getConversationId());
        map.put("IYZI_FORM_INIT_RESULT_ERROR_CODE", checkoutFormInitialize.getErrorCode());
        map.put("IYZI_FORM_INIT_RESULT_ERROR_GROUP", checkoutFormInitialize.getErrorGroup());
        map.put("IYZI_FORM_INIT_RESULT_ERROR_MESSAGE", checkoutFormInitialize.getErrorMessage());
        map.put("IYZI_FORM_INIT_RESULT_LOCALE", checkoutFormInitialize.getLocale());
        map.put("IYZI_FORM_INIT_RESULT_TOKEN", checkoutFormInitialize.getToken());
        map.put("IYZI_FORM_INIT_RESULT_SYSTEM_TIME", checkoutFormInitialize.getSystemTime()+"");
        map.put("IYZI_FORM_INIT_RESULT_TOKEN_EXPRIE_TIME", checkoutFormInitialize.getTokenExpireTime()+"");
        return map;
    }

/*
    public static CheckoutFormInitialize checkoutFormInitialize(){
        {
            CreateCheckoutFormInitializeRequest request = new CreateCheckoutFormInitializeRequest();
            request.setLocale(Locale.TR.getValue());
            request.setConversationId("123456789");
            request.setPrice(new BigDecimal("1"));
            request.setPaidPrice(new BigDecimal("1.2"));
            request.setCurrency(Currency.TRY.name());
            request.setBasketId("B67832");
            request.setPaymentGroup(PaymentGroup.PRODUCT.name());
            request.setCallbackUrl("http://local.kapsulstore.com:9001/api/iyzico/transaction/callback");

            List<Integer> enabledInstallments = new ArrayList<Integer>();
            enabledInstallments.add(2);
            enabledInstallments.add(3);
            enabledInstallments.add(6);
            enabledInstallments.add(9);
            request.setEnabledInstallments(enabledInstallments);

            Buyer buyer = new Buyer();
            buyer.setId("BY789");
            buyer.setName("John");
            buyer.setSurname("Doe");
            buyer.setGsmNumber("+905350000000");
            buyer.setEmail("email@email.com");
            buyer.setIdentityNumber("74300864791");
            buyer.setLastLoginDate("2015-10-05 12:43:35");
            buyer.setRegistrationDate("2013-04-21 15:12:09");
            buyer.setRegistrationAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
            buyer.setIp("85.34.78.112");
            buyer.setCity("Istanbul");
            buyer.setCountry("Turkey");
            buyer.setZipCode("34732");
            request.setBuyer(buyer);

            Address shippingAddress = new Address();
            shippingAddress.setContactName("Jane Doe");
            shippingAddress.setCity("Istanbul");
            shippingAddress.setCountry("Turkey");
            shippingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
            shippingAddress.setZipCode("34742");
            request.setShippingAddress(shippingAddress);

            Address billingAddress = new Address();
            billingAddress.setContactName("Jane Doe");
            billingAddress.setCity("Istanbul");
            billingAddress.setCountry("Turkey");
            billingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
            billingAddress.setZipCode("34742");
            request.setBillingAddress(billingAddress);

            List<BasketItem> basketItems = new ArrayList<BasketItem>();
            BasketItem firstBasketItem = new BasketItem();
            firstBasketItem.setId("BI101");
            firstBasketItem.setName("Binocular");
            firstBasketItem.setCategory1("Collectibles");
            firstBasketItem.setCategory2("Accessories");
            firstBasketItem.setItemType(BasketItemType.PHYSICAL.name());
            firstBasketItem.setPrice(new BigDecimal("0.3"));
            basketItems.add(firstBasketItem);

            BasketItem secondBasketItem = new BasketItem();
            secondBasketItem.setId("BI102");
            secondBasketItem.setName("Game code");
            secondBasketItem.setCategory1("Game");
            secondBasketItem.setCategory2("Online Game Items");
            secondBasketItem.setItemType(BasketItemType.VIRTUAL.name());
            secondBasketItem.setPrice(new BigDecimal("0.5"));
            basketItems.add(secondBasketItem);

            BasketItem thirdBasketItem = new BasketItem();
            thirdBasketItem.setId("BI103");
            thirdBasketItem.setName("Usb");
            thirdBasketItem.setCategory1("Electronics");
            thirdBasketItem.setCategory2("Usb / Cable");
            thirdBasketItem.setItemType(BasketItemType.PHYSICAL.name());
            thirdBasketItem.setPrice(new BigDecimal("0.2"));
            basketItems.add(thirdBasketItem);
            request.setBasketItems(basketItems);

            CheckoutFormInitialize checkoutFormInitialize = CheckoutFormInitialize.create(request, IyzicoBuilder.getTestOptions());

            System.out.println(checkoutFormInitialize);
            return checkoutFormInitialize;
        }
    }
    */
/*
    public CreatePaymentRequest buildPaymentRequest(OrderWrapper order){


        CreatePaymentRequest request = new CreatePaymentRequest();
        request.setLocale(Locale.TR.getValue());
        request.setConversationId("123456789");
        request.setPrice(new BigDecimal("1"));
        request.setPaidPrice(new BigDecimal("1.2"));
        request.setCurrency(order.getTotal().getCurrency().getCurrencyCode());
        request.setInstallment(1);
        request.setBasketId("B67832");
        request.setPaymentChannel(PaymentChannel.WEB.name());
        request.setPaymentGroup(PaymentGroup.PRODUCT.name());

        PaymentCard paymentCard = new PaymentCard();
        paymentCard.setCardHolderName("John Doe");
        paymentCard.setCardNumber("5528790000000008");
        paymentCard.setExpireMonth("12");
        paymentCard.setExpireYear("2030");
        paymentCard.setCvc("123");
        paymentCard.setRegisterCard(0);
        request.setPaymentCard(paymentCard);





        Buyer buyer = new Buyer();
        buyer.setId("BY789");
        buyer.setName("John");
        buyer.setSurname("Doe");
        buyer.setGsmNumber("+905350000000");
        buyer.setEmail("email@email.com");
        buyer.setIdentityNumber("74300864791");
        buyer.setLastLoginDate("2015-10-05 12:43:35");
        buyer.setRegistrationDate("2013-04-21 15:12:09");
        buyer.setRegistrationAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        buyer.setIp("85.34.78.112");
        buyer.setCity("Istanbul");
        buyer.setCountry("Turkey");
        buyer.setZipCode("34732");
        request.setBuyer(buyer);

        Address shippingAddress = new Address();
        shippingAddress.setContactName("Jane Doe");
        shippingAddress.setCity("Istanbul");
        shippingAddress.setCountry("Turkey");
        shippingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        shippingAddress.setZipCode("34742");
        request.setShippingAddress(shippingAddress);

        Address billingAddress = new Address();
        billingAddress.setContactName("Jane Doe");
        billingAddress.setCity("Istanbul");
        billingAddress.setCountry("Turkey");
        billingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        billingAddress.setZipCode("34742");
        request.setBillingAddress(billingAddress);

        List<BasketItem> basketItems = new ArrayList<>();
        BasketItem firstBasketItem = new BasketItem();
        firstBasketItem.setId("BI101");
        firstBasketItem.setName("Binocular");
        firstBasketItem.setCategory1("Collectibles");
        firstBasketItem.setCategory2("Accessories");
        firstBasketItem.setItemType(BasketItemType.PHYSICAL.name());
        firstBasketItem.setPrice(new BigDecimal("0.3"));
        basketItems.add(firstBasketItem);

        BasketItem secondBasketItem = new BasketItem();
        secondBasketItem.setId("BI102");
        secondBasketItem.setName("Game code");
        secondBasketItem.setCategory1("Game");
        secondBasketItem.setCategory2("Online Game Items");
        secondBasketItem.setItemType(BasketItemType.VIRTUAL.name());
        secondBasketItem.setPrice(new BigDecimal("0.5"));
        basketItems.add(secondBasketItem);

        BasketItem thirdBasketItem = new BasketItem();
        thirdBasketItem.setId("BI103");
        thirdBasketItem.setName("Usb");
        thirdBasketItem.setCategory1("Electronics");
        thirdBasketItem.setCategory2("Usb / Cable");
        thirdBasketItem.setItemType(BasketItemType.PHYSICAL.name());
        thirdBasketItem.setPrice(new BigDecimal("0.2"));
        basketItems.add(thirdBasketItem);
        request.setBasketItems(basketItems);

        Payment payment = Payment.create(request, IyzicoBuilder.getTestOptions());
        return request;

    }
    */
    /*
    public static CheckoutFormInitialize orderToCheckoutFormInitialize(Order order){
        CreateCheckoutFormInitializeRequest request = new CreateCheckoutFormInitializeRequest();
        request.setLocale(Locale.TR.getValue());
        request.setConversationId(order.getId().toString());
        request.setPrice(order.getTotal().getAmount());
        request.setPaidPrice(order.getSubTotal().getAmount());
        request.setCurrency(Currency.TRY.name());
        request.setPaymentGroup(PaymentGroup.PRODUCT.name());
        request.setCallbackUrl("http://local.kapsulstore.com:9001/api/iyzico/transaction/callback");

        List<Integer> enabledInstallments = new ArrayList<Integer>();
        enabledInstallments.add(2);
        enabledInstallments.add(3);
        enabledInstallments.add(6);
        enabledInstallments.add(9);
        request.setEnabledInstallments(enabledInstallments);

        Buyer buyer = new Buyer();
        buyer.setId(order.getCustomer().getId().toString());
        buyer.setName(order.getCustomer().getFirstName());
        buyer.setSurname(order.getCustomer().getLastName());

        buyer.setGsmNumber(order.getFulfillmentGroups().get(0).getAddress().getPrimaryPhone().);
        buyer.setEmail("email@email.com");
        buyer.setIdentityNumber("74300864791");
        buyer.setLastLoginDate("2015-10-05 12:43:35");
        buyer.setRegistrationDate("2013-04-21 15:12:09");
        buyer.setRegistrationAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        buyer.setIp("85.34.78.112");
        buyer.setCity("Istanbul");
        buyer.setCountry("Turkey");
        buyer.setZipCode("34732");
        request.setBuyer(buyer);

        Address shippingAddress = new Address();
        shippingAddress.setContactName("Jane Doe");
        shippingAddress.setCity("Istanbul");
        shippingAddress.setCountry("Turkey");
        shippingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        shippingAddress.setZipCode("34742");
        request.setShippingAddress(shippingAddress);

        Address billingAddress = new Address();
        billingAddress.setContactName("Jane Doe");
        billingAddress.setCity("Istanbul");
        billingAddress.setCountry("Turkey");
        billingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        billingAddress.setZipCode("34742");
        request.setBillingAddress(billingAddress);

        List<BasketItem> basketItems = new ArrayList<BasketItem>();
        BasketItem firstBasketItem = new BasketItem();
        firstBasketItem.setId("BI101");
        firstBasketItem.setName("Binocular");
        firstBasketItem.setCategory1("Collectibles");
        firstBasketItem.setCategory2("Accessories");
        firstBasketItem.setItemType(BasketItemType.PHYSICAL.name());
        firstBasketItem.setPrice(new BigDecimal("0.3"));
        basketItems.add(firstBasketItem);

        BasketItem secondBasketItem = new BasketItem();
        secondBasketItem.setId("BI102");
        secondBasketItem.setName("Game code");
        secondBasketItem.setCategory1("Game");
        secondBasketItem.setCategory2("Online Game Items");
        secondBasketItem.setItemType(BasketItemType.VIRTUAL.name());
        secondBasketItem.setPrice(new BigDecimal("0.5"));
        basketItems.add(secondBasketItem);

        BasketItem thirdBasketItem = new BasketItem();
        thirdBasketItem.setId("BI103");
        thirdBasketItem.setName("Usb");
        thirdBasketItem.setCategory1("Electronics");
        thirdBasketItem.setCategory2("Usb / Cable");
        thirdBasketItem.setItemType(BasketItemType.PHYSICAL.name());
        thirdBasketItem.setPrice(new BigDecimal("0.2"));
        basketItems.add(thirdBasketItem);
        request.setBasketItems(basketItems);

        CheckoutFormInitialize checkoutFormInitialize = CheckoutFormInitialize.create(request, IyzicoBuilder.getTestOptions());

        System.out.println(checkoutFormInitialize);
        return checkoutFormInitialize;
    }

    }
*/
}
