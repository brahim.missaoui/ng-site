package com.kapsulstore.ngsite.web.rest.checkout;

import com.iyzipay.model.CheckoutFormInitialize;
import com.iyzipay.model.Locale;
import com.iyzipay.request.RetrieveCheckoutFormRequest;
import com.kapsulstore.ngsite.service.payment.gateway.iyzico.IyzipayPaymentGatewayType;
import com.kapsulstore.ngsite.service.payment.gateway.iyzico.IyzipaySamplePaymentGatewayHostedController;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.i18n.service.ISOService;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.AddressDTO;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.*;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkRestController;
import org.broadleafcommerce.common.web.payment.controller.CustomerPaymentGatewayAbstractController;
import org.broadleafcommerce.common.web.payment.controller.PaymentGatewayAbstractController;
import org.broadleafcommerce.core.checkout.service.CheckoutService;
import org.broadleafcommerce.core.checkout.service.exception.CheckoutException;
import org.broadleafcommerce.core.checkout.service.workflow.CheckoutResponse;
import org.broadleafcommerce.core.order.domain.*;
import org.broadleafcommerce.core.order.service.FulfillmentGroupService;
import org.broadleafcommerce.core.order.service.FulfillmentOptionService;
import org.broadleafcommerce.core.order.service.OrderMultishipOptionService;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.core.payment.service.OrderPaymentService;
import org.broadleafcommerce.core.payment.service.OrderToPaymentRequestDTOService;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.core.web.checkout.validator.*;
import org.broadleafcommerce.core.web.controller.checkout.BroadleafCheckoutControllerExtensionManager;
import org.broadleafcommerce.profile.core.domain.Address;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.domain.CustomerAddress;
import org.broadleafcommerce.profile.core.domain.Phone;
import org.broadleafcommerce.profile.core.service.*;
import org.broadleafcommerce.vendor.sample.web.controller.SamplePaymentGatewayCustomerPaymentController;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Currency;
import java.util.List;
import java.util.Map;

/**
 * Created by mehmet on 7/14/17.
 */
@Controller("ksIyzipayPaymentGatewayHostedController")
//@Transactional(readOnly = true)
@RequestMapping(value = "/api/iyzipay/transaction",produces = { MediaType.APPLICATION_XHTML_XML_VALUE })
@FrameworkRestController
@FrameworkMapping(
    value = {"/api/iyzipay/transaction"},
    produces = {"application/json"}
)
public class KSIyzipayPaymentGatewayHostedController extends IyzipaySamplePaymentGatewayHostedController{
    protected static final Log LOG = LogFactory.getLog(KSIyzipayPaymentGatewayHostedController.class);
    protected static final String GATEWAY_CONTEXT_KEY = "/api/iyzipay/transaction";
    public static final String CUSTOMER_ADDRESS_NAME_LAST_ORDER_BILLING = "Son Fatura Adresim";
    public static final String CUSTOMER_ADDRESS_NAME_LAST_ORDER_SHIPPING = "Son Teslimat Adresim";

    @Resource(name = "iyzipayOrderToPaymentRequestDTOService")
    protected OrderToPaymentRequestDTOService paymentRequestDTOService;
    @Resource(name = "iyzipayPaymentGatewayHostedService")
    private PaymentGatewayHostedService paymentGatewayHostedService;

    @Resource(name = "iyzipayPaymentGatewayHostedWebResponseService")
    protected PaymentGatewayWebResponseService paymentGatewayWebResponseService;
    @Resource(name = "iyzipayPaymentGatewayHostedConfiguration")
    protected PaymentGatewayConfiguration paymentGatewayConfiguration;

    @Resource(name = "ksPaymentGatewayCheckoutService")
    protected PaymentGatewayCheckoutService paymentGatewayCheckoutService;

    @Resource(name = "blOrderService")
    protected OrderService orderService;
    @Resource(name = "blOrderPaymentService")
    protected OrderPaymentService orderPaymentService;
    @Resource(name = "blOrderToPaymentRequestDTOService")
    protected OrderToPaymentRequestDTOService dtoTranslationService;
    @Resource(name = "blFulfillmentGroupService")
    protected FulfillmentGroupService fulfillmentGroupService;
    @Resource(name = "blFulfillmentOptionService")
    protected FulfillmentOptionService fulfillmentOptionService;
    @Resource(name = "blCheckoutService")
    protected CheckoutService checkoutService;
    @Resource(name = "blCustomerService")
    protected CustomerService customerService;
    @Resource(name = "blCustomerPaymentService")
    protected CustomerPaymentService customerPaymentService;
    @Resource(name = "blStateService")
    protected StateService stateService;
    @Resource(name = "blCountryService")
    protected CountryService countryService;
    @Resource(name = "blCountrySubdivisionService")
    protected CountrySubdivisionService countrySubdivisionService;
    @Resource(name = "blISOService")
    protected ISOService isoService;
    @Resource(name = "blCustomerAddressService")
    protected CustomerAddressService customerAddressService;
    @Resource(name = "blAddressService")
    protected AddressService addressService;
    @Resource(name = "blPhoneService")
    protected PhoneService phoneService;
    @Resource(name = "blOrderMultishipOptionService")
    protected OrderMultishipOptionService orderMultishipOptionService;
    @Resource(name = "blShippingInfoFormValidator")
    protected ShippingInfoFormValidator shippingInfoFormValidator;
    @Resource(name = "blBillingInfoFormValidator")
    protected BillingInfoFormValidator billingInfoFormValidator;
    @Resource(name = "blGiftCardInfoFormValidator")
    protected GiftCardInfoFormValidator giftCardInfoFormValidator;
    @Resource(name = "blMultishipAddAddressFormValidator")
    protected MultishipAddAddressFormValidator multishipAddAddressFormValidator;
    @Resource(name = "blOrderInfoFormValidator")
    protected OrderInfoFormValidator orderInfoFormValidator;
    @Resource(name = "blCheckoutControllerExtensionManager")
    protected BroadleafCheckoutControllerExtensionManager checkoutControllerExtensionManager;

    public PaymentGatewayWebResponseService getWebResponseService() { return this.paymentGatewayWebResponseService; }

    public PaymentGatewayConfiguration getConfiguration() {
        return this.paymentGatewayConfiguration;
    }




    @Override
    public Long applyPaymentToOrder(PaymentResponseDTO responseDTO) throws IllegalArgumentException {
        String token = responseDTO.getResponseMap().get("TOKEN");

        Order orderByOrderNumber = orderService.findOrderByOrderNumber(token);

        System.out.println("transactionCallback:" + token);
        RetrieveCheckoutFormRequest irequest = new RetrieveCheckoutFormRequest();
        irequest.setLocale(Locale.TR.getValue());
        irequest.setConversationId(orderByOrderNumber.getId().toString());
        irequest.setToken(token);
        com.iyzipay.model.CheckoutForm checkoutForm = com.iyzipay.model.CheckoutForm.retrieve(irequest, IyzicoBuilder.getOptions());

        Money amount = new Money(checkoutForm.getPaidPrice(), Currency.getInstance(checkoutForm.getCurrency()));
        responseDTO.amount(amount);

        responseDTO.completeCheckoutOnCallback(true);
        responseDTO.orderId(orderByOrderNumber.getId().toString());
        responseDTO.responseMap("TOKEN", token);
        responseDTO.responseMap("MESSAGE", checkoutForm.getErrorMessage());
        responseDTO.orderId(orderByOrderNumber.getId().toString());

        return super.applyPaymentToOrder(responseDTO);
    }

    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    @Transactional("blTransactionManager")
    public RedirectView transactionCallback(Model model, HttpServletRequest httpRequest, HttpServletResponse response, RedirectAttributes redirectAttributes, @RequestParam("token") String token) throws PaymentException {
        return new RedirectView(super.process(null, httpRequest, redirectAttributes));
        //return new RedirectView("/#/pages/payment-thanks");
    }

    @Override
    protected String getErrorViewRedirect() {
        return "/#/pages/payment-error";
    }

    @Override
    protected String getConfirmationViewRedirect(String orderNumber) {
        return "/#/pages/payment-thanks";
        //return "/#/pages/payment-thanks/" + orderNumber;
    }

    @RequestMapping(value="/anonymous/{cartId}", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE } )
    @Transactional("blTransactionManager")
    public CheckoutFormInitialize anonymousCheckout(HttpServletRequest request, HttpServletResponse response, @PathVariable("cartId") Long cartId, @RequestBody AnonymousCheckoutModel checkoutModel) throws PricingException, PaymentException {
        Order cart = this.orderService.findOrderById(cartId);
        Order updatedOrder = updateOrderFromAnonymousCheckout(cart, checkoutModel);
        //Order updatedOrder = cart;
        PaymentRequestDTO paymentRequestDTO = paymentRequestDTOService.translateOrder(updatedOrder);

        if (paymentRequestDTO != null) {
            try {
                if ( request.getParameterMap().containsKey("complete") ) {
                    Boolean completeCheckout = Boolean.parseBoolean(request.getParameter("complete"));
                    paymentRequestDTO.completeCheckoutOnCallback(completeCheckout);
                }

                PaymentResponseDTO responseDTO = this.paymentGatewayHostedService.requestHostedEndpoint(paymentRequestDTO);
                CheckoutFormInitialize checkoutFormInitialize = new CheckoutFormInitialize();
                String url = responseDTO.getResponseMap().get("HOSTED_REDIRECT_URL").toString();
                String token = responseDTO.getResponseMap().get("TOKEN").toString();
                updatedOrder.setOrderNumber(token);
                this.orderService.save(updatedOrder,false);


                checkoutFormInitialize.setPaymentPageUrl(url);


                if (LOG.isTraceEnabled()) {
                    LOG.trace("Redirecting to PayPal Express with URL: " + url);
                }

                return checkoutFormInitialize;
            } catch (Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.error("Unable to Create the PayPal Express Redirect Link.", e);
                }
            }
        }

        throw new PaymentException("Unable to Create the PayPal Express Redirect Link. The Request DTO is null");

    }


    private Customer updateCustomerFromCart(Order cart, AnonymousCheckoutModel anonymousCheckoutModel) throws PricingException {
        Customer customerByEmail = this.customerService.readCustomerByEmail(anonymousCheckoutModel.customerEMail);
        if( customerByEmail!=null ){
            customerByEmail.setFirstName(anonymousCheckoutModel.billingAddress.name);
            customerByEmail.setLastName(anonymousCheckoutModel.billingAddress.surname);
            return customerService.saveCustomer(customerByEmail);
        }else{
            cart.getCustomer().setFirstName(anonymousCheckoutModel.billingAddress.name);
            cart.getCustomer().setLastName(anonymousCheckoutModel.billingAddress.surname);
            return customerService.saveCustomer(cart.getCustomer());
        }
    }

    private void updateCustomerAddressesFromAnonymousModel(Order cart, AnonymousCheckoutModel anonymousCheckoutModel) throws PricingException {
        this.updateCustomerAddressByNameFromCheckoutAddress(cart.getCustomer(), anonymousCheckoutModel.billingAddress,CUSTOMER_ADDRESS_NAME_LAST_ORDER_BILLING);
        this.updateCustomerAddressByNameFromCheckoutAddress(cart.getCustomer(), anonymousCheckoutModel.billingAddress,anonymousCheckoutModel.billingAddress.name);
        this.updateCustomerAddressByNameFromCheckoutAddress(cart.getCustomer(), anonymousCheckoutModel.shippingAddress,CUSTOMER_ADDRESS_NAME_LAST_ORDER_SHIPPING);
        this.updateCustomerAddressByNameFromCheckoutAddress(cart.getCustomer(), anonymousCheckoutModel.shippingAddress,anonymousCheckoutModel.shippingAddress.name);
    }

    private CustomerAddress updateCustomerAddressByNameFromCheckoutAddress(Customer customer, CheckoutAddress checkoutAddress, String addressName){
        List<CustomerAddress> customerAddresses = customer.getCustomerAddresses();

        boolean isAddressFoundAndUpdated = false;
        CustomerAddress customerAddress = getAddressOfCustomerByName(customer, addressName);
        if(customerAddress!=null){
            Address address = customerAddress.getAddress();
            populateToAddress(address, checkoutAddress);
            return this.customerAddressService.saveCustomerAddress(customerAddress);
        }else{
            CustomerAddress newCustomerAddress = customerAddressService.create();
            newCustomerAddress.setAddressName(addressName);
            Address newAddress = addressService.create();
            populateToAddress(newAddress, checkoutAddress);
            newAddress.setIsoCountryAlpha2(this.isoService.findISOCountryByAlpha2Code("TR"));
            this.addressService.populateAddressISOCountrySub(newAddress);
            newAddress.setCountry(null);

            newCustomerAddress.setAddress(newAddress);
            newCustomerAddress.setCustomer(customer);
            customer.getCustomerAddresses().add(newCustomerAddress);

            return this.customerAddressService.saveCustomerAddress(newCustomerAddress);

        }
    }

    private void populateToAddress(Address address, CheckoutAddress checkoutAddress) {
        address.setFirstName(checkoutAddress.name);
        address.setLastName(checkoutAddress.surname);
        address.setAddressLine1(checkoutAddress.address1);
        address.setAddressLine2(checkoutAddress.address2);
        address.setPostalCode(checkoutAddress.postalCode);
        address.setCity(checkoutAddress.city);
        address.setEmailAddress(checkoutAddress.eMail);
        Phone phone = this.phoneService.create();
        phone.setPhoneNumber(checkoutAddress.phone);
        address.setPhonePrimary(phone);
    }

    private CustomerAddress getLastShippingAddressOfCustomer(Customer customer){
        return getAddressOfCustomerByName(customer, CUSTOMER_ADDRESS_NAME_LAST_ORDER_SHIPPING);
    }
    private CustomerAddress getAddressOfCustomerByName(Customer customer, String addressName){
        List<CustomerAddress> customerAddresses = customer.getCustomerAddresses();
        for (CustomerAddress customerAddress: customerAddresses) {
            if(StringUtils.equalsAnyIgnoreCase(customerAddress.getAddressName(), addressName)){
                return customerAddress;
            }
        }
        return null;
    }
    private Order updateOrderFromAnonymousCheckout(Order cart, AnonymousCheckoutModel anonymousCheckoutModel) throws PricingException {
        cart.setEmailAddress(anonymousCheckoutModel.customerEMail);
        Customer updatedCustomer = updateCustomerFromCart(cart, anonymousCheckoutModel);
        this.updateCustomerAddressesFromAnonymousModel(cart,anonymousCheckoutModel);
        cart.setCustomer(updatedCustomer);
        customerService.saveCustomer(updatedCustomer);


        CustomerAddress lastShippingAddressOfCustomer = getLastShippingAddressOfCustomer(updatedCustomer);
        if(lastShippingAddressOfCustomer==null){

        }
        Address shippingAddress = addressService.copyAddress(lastShippingAddressOfCustomer.getAddress());


        FulfillmentGroup fulfillmentGroup1 = this.fulfillmentGroupService.createEmptyFulfillmentGroup();
        fulfillmentGroup1.setAddress(shippingAddress);

        fulfillmentGroup1.setOrder(cart);

        cart.getFulfillmentGroups().add(fulfillmentGroup1);
        return cart;
    }


}
