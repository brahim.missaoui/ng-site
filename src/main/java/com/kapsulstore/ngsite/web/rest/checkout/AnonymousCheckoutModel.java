package com.kapsulstore.ngsite.web.rest.checkout;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by mehmet on 7/11/17.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AnonymousCheckoutModel {
    public String customerEMail;
    public CheckoutAddress shippingAddress;
    public CheckoutAddress billingAddress;
}

