package com.kapsulstore.ngsite.web.rest.checkout;

import com.iyzipay.HttpClient;
import com.iyzipay.Options;
import com.iyzipay.model.CheckoutFormInitialize;
import com.iyzipay.request.CreateCheckoutFormInitializeRequest;

import java.util.Map;

/**
 * Created by mehmet on 7/17/17.
 */
public class KSIyzipayCheckoutFormInitialize extends CheckoutFormInitialize {
    public static CheckoutFormInitialize create(CreateCheckoutFormInitializeRequest request, Options options) {

        Map<String, String> httpHeaders = getHttpHeaders(request, options);
        System.out.println("KSIyzipayCheckoutFormInitialize.create:httpHeaders" + httpHeaders);
        System.out.println("KSIyzipayCheckoutFormInitialize.create:request" + request);
        return (CheckoutFormInitialize) HttpClient.create().post(options.getBaseUrl() + "/payment/iyzipos/checkoutform/initialize/auth/ecom", httpHeaders, request, CheckoutFormInitialize.class);
    }
}
