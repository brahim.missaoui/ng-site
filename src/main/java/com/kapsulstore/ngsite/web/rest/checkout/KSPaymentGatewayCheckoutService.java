package com.kapsulstore.ngsite.web.rest.checkout;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.checkout.service.exception.CheckoutException;
import org.broadleafcommerce.core.checkout.service.workflow.CheckoutResponse;
import org.broadleafcommerce.core.order.domain.NullOrderImpl;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.payment.service.DefaultPaymentGatewayCheckoutService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Created by mehmet on 7/14/17.
 */
@Service("ksPaymentGatewayCheckoutService")
@Primary
public class KSPaymentGatewayCheckoutService extends DefaultPaymentGatewayCheckoutService {

    protected static final Log LOG = LogFactory.getLog(KSPaymentGatewayCheckoutService.class);
    @Override
    public String initiateCheckout(Long orderId) throws Exception {
        Order order = this.orderService.findOrderById(orderId, false);
        if(order != null && !(order instanceof NullOrderImpl)) {
            CheckoutResponse response;
            try {
                response = this.checkoutService.performCheckout(order);
            } catch (CheckoutException var5) {
                throw new Exception(var5);
            }

            if(response.getOrder().getOrderNumber() == null) {
                LOG.error("Order Number for Order ID: " + order.getId() + " is null.");
            }

            return response.getOrder().getOrderNumber();
        } else {
            throw new IllegalArgumentException("Could not order with id " + orderId);
        }
    }
    /*
    public String processIyzipayCheckoutForm(HttpServletRequest request, com.iyzipay.model.CheckoutForm checkoutForm, RedirectAttributes redirectAttributes ) throws PaymentException {
        Object orderPaymentId = null;

        try {
            PaymentResponseDTO responseDTO = this.getWebResponseService().translateWebResponse(request);
            if(LOG.isTraceEnabled()) {
                LOG.trace("HTTPRequest translated to Raw Response: " + responseDTO.getRawResponse());
            }

            this.applyPaymentToOrder(responseDTO);
            if(!responseDTO.isSuccessful()) {
                if(LOG.isTraceEnabled()) {
                    LOG.trace("The Response DTO is marked as unsuccessful. Delegating to the payment module to handle an unsuccessful transaction");
                }

                this.handleUnsuccessfulTransaction(null, redirectAttributes, responseDTO);
                return this.getErrorViewRedirect();
            } else if(!responseDTO.isValid()) {
                throw new PaymentException("The validity of the response cannot be confirmed.Check the Tamper Proof Seal for more details.");
            } else {
                String orderId = responseDTO.getOrderId();
                if(orderId == null) {
                    throw new RuntimeException("Order ID must be set on the Payment Response DTO");
                } else if(responseDTO.isCompleteCheckoutOnCallback()) {
                    if(LOG.isTraceEnabled()) {
                        LOG.trace("The Response DTO for this Gateway is configured to complete checkout on callback. Initiating Checkout with Order ID: " + orderId);
                    }

                    String orderNumber = this.initiateCheckout(Long.valueOf(Long.parseLong(orderId)));
                    return this.getConfirmationViewRedirect(orderNumber);
                } else {
                    if(LOG.isTraceEnabled()) {
                        LOG.trace("The Gateway is configured to not complete checkout. Redirecting to the Order Review Page for Order ID: " + orderId);
                    }

                    return this.getOrderReviewRedirect();
                }
            }
        } catch (Exception var8) {
            if(LOG.isErrorEnabled()) {
                LOG.error("HTTPRequest - " + this.webResponsePrintService.printRequest(request));
                LOG.error("An exception was caught either from processing the response and applying the payment to the order, or an activity in the checkout workflow threw an exception. Attempting to mark the payment as invalid and delegating to the payment module to handle any other exception processing", var8);
            }

            if(this.paymentGatewayCheckoutService != null && orderPaymentId != null) {
                this.paymentGatewayCheckoutService.markPaymentAsInvalid((Long)orderPaymentId);
            }

            this.handleProcessingException(var8, redirectAttributes);
            return this.getErrorViewRedirect();
        }
    }
*/
}
