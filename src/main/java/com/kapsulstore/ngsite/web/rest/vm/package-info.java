/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kapsulstore.ngsite.web.rest.vm;
