package com.kapsulstore.ngsite.web.rest.checkout;

import com.iyzipay.model.CheckoutFormInitialize;
import org.broadleafcommerce.common.exception.ServiceException;
import org.broadleafcommerce.common.payment.PaymentGatewayType;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.service.PaymentGatewayCheckoutService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkRestController;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.payment.domain.OrderPayment;
import org.broadleafcommerce.core.payment.domain.PaymentTransaction;
import org.broadleafcommerce.core.payment.domain.secure.Referenced;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.core.web.checkout.model.*;
import org.broadleafcommerce.core.web.controller.checkout.BroadleafCheckoutController;
import org.broadleafcommerce.core.web.order.CartState;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mehmet on 7/15/17.
 */
@Controller
@RequestMapping(value = "/api/checkout/cod")
@FrameworkRestController
@FrameworkMapping(
    value = {"/api/checkout/cod"},
    produces = {"application/json"}
)
public class KSCodCheckoutController extends BroadleafCheckoutController {
    @Resource(name = "ksPaymentGatewayCheckoutService")
    protected PaymentGatewayCheckoutService paymentGatewayCheckoutService;

    //@RequestMapping("/checkout")
    public String checkout(HttpServletRequest request, HttpServletResponse response, Model model,
                           @ModelAttribute("orderInfoForm") OrderInfoForm orderInfoForm,
                           @ModelAttribute("shippingInfoForm") ShippingInfoForm shippingForm,
                           @ModelAttribute("billingInfoForm") BillingInfoForm billingForm,
                           @ModelAttribute("giftCardInfoForm") GiftCardInfoForm giftCardInfoForm,
                           @ModelAttribute("customerCreditInfoForm") CustomerCreditInfoForm customerCreditInfoForm,
                           RedirectAttributes redirectAttributes) {
        return super.checkout(request, response, model, redirectAttributes);
    }

    //@RequestMapping(value = "/checkout/savedetails", method = RequestMethod.POST)
    public String saveGlobalOrderDetails(HttpServletRequest request, Model model,
                                         @ModelAttribute("shippingInfoForm") ShippingInfoForm shippingForm,
                                         @ModelAttribute("billingInfoForm") BillingInfoForm billingForm,
                                         @ModelAttribute("giftCardInfoForm") GiftCardInfoForm giftCardInfoForm,
                                         @ModelAttribute("orderInfoForm") OrderInfoForm orderInfoForm, BindingResult result) throws ServiceException {
        return super.saveGlobalOrderDetails(request, model, orderInfoForm, result);
    }

    //@RequestMapping(value = "/checkout/complete", method = RequestMethod.POST)
    public String processCompleteCheckoutOrderFinalized(RedirectAttributes redirectAttributes) throws PaymentException {
        return super.processCompleteCheckoutOrderFinalized(redirectAttributes);
    }

    @RequestMapping(value = "/complete/{cartId}", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    @Transactional("blTransactionManager")
    public CheckoutFormInitialize processPassthroughCheckout(HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes redirectAttributes, @PathVariable("cartId") Long cartId, @RequestBody AnonymousCheckoutModel checkoutModel) {
        try {
            String checkout = this.processPassthroughCheckout(cartId, redirectAttributes, PaymentType.COD);
        } catch (PaymentException e) {
            e.printStackTrace();
        } catch (PricingException e) {
            e.printStackTrace();
        }
        CheckoutFormInitialize checkoutFormInitialize = new CheckoutFormInitialize();
        checkoutFormInitialize.setStatus("success");
        return checkoutFormInitialize;
    }

    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
    }


    public String processPassthroughCheckout(Long cartId, RedirectAttributes redirectAttributes, PaymentType paymentType) throws PaymentException, PricingException {
        Order cart = CartState.getCart();
        //cart = this.orderService.findOrderById(cartId);
        List<OrderPayment> paymentsToInvalidate = new ArrayList();
        Iterator var5 = cart.getPayments().iterator();

        while(true) {
            while(true) {
                OrderPayment payment;
                do {
                    if(!var5.hasNext()) {
                        var5 = paymentsToInvalidate.iterator();

                        while(var5.hasNext()) {
                            payment = (OrderPayment)var5.next();
                            cart.getPayments().remove(payment);
                            if(this.paymentGatewayCheckoutService != null) {
                                this.paymentGatewayCheckoutService.markPaymentAsInvalid(payment.getId());
                            }
                        }

                        OrderPayment passthroughPayment = this.orderPaymentService.create();
                        passthroughPayment.setType(paymentType);
                        passthroughPayment.setPaymentGatewayType(PaymentGatewayType.PASSTHROUGH);
                        passthroughPayment.setAmount(cart.getTotalAfterAppliedPayments());
                        passthroughPayment.setOrder(cart);
                        PaymentTransaction transaction = this.orderPaymentService.createTransaction();
                        transaction.setAmount(cart.getTotalAfterAppliedPayments());
                        transaction.setRawResponse("Passthrough Payment");
                        transaction.setSuccess(Boolean.valueOf(true));
                        transaction.setType(PaymentTransactionType.AUTHORIZE_AND_CAPTURE);
                        transaction.getAdditionalFields().put("PASSTHROUGH_PAYMENT_TYPE", paymentType.getType());
                        transaction.setOrderPayment(passthroughPayment);
                        passthroughPayment.addTransaction(transaction);
                        this.orderService.addPaymentToOrder(cart, passthroughPayment, (Referenced)null);
                        this.orderService.save(cart, Boolean.valueOf(true));
                        return this.processCompleteCheckoutOrderFinalized(redirectAttributes);
                    }

                    payment = (OrderPayment)var5.next();
                } while(!payment.isActive());

                if(payment.getTransactions() != null && !payment.getTransactions().isEmpty()) {
                    Iterator var7 = payment.getTransactions().iterator();

                    while(var7.hasNext()) {
                        PaymentTransaction transaction = (PaymentTransaction)var7.next();
                        if(!PaymentTransactionType.UNCONFIRMED.equals(transaction.getType())) {
                            paymentsToInvalidate.add(payment);
                        }
                    }
                } else {
                    paymentsToInvalidate.add(payment);
                }
            }
        }
    }


}

