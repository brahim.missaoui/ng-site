package com.kapsulstore.ngsite.web.rest.menu;

import org.broadleafcommerce.menu.domain.Menu;
import org.broadleafcommerce.menu.dto.MenuItemDTO;
import org.broadleafcommerce.menu.processor.MenuProcessor;
import org.broadleafcommerce.menu.processor.MenuProcessorExtensionManager;
import org.broadleafcommerce.menu.service.MenuService;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.broadleafcommerce.rest.api.wrapper.CategoriesWrapper;
import com.broadleafcommerce.rest.api.wrapper.CategoryAttributeWrapper;
import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.InventoryWrapper;
import com.broadleafcommerce.rest.api.wrapper.MediaWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductAttributeWrapper;
import com.broadleafcommerce.rest.api.wrapper.ProductWrapper;
import com.broadleafcommerce.rest.api.wrapper.RelatedProductWrapper;
import com.broadleafcommerce.rest.api.wrapper.SearchResultsWrapper;
import com.broadleafcommerce.rest.api.wrapper.SkuAttributeWrapper;
import com.broadleafcommerce.rest.api.wrapper.SkuWrapper;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * This is a reference REST API endpoint for catalog. This can be modified, used as is, or removed.
 * The purpose is to provide an out of the box RESTful catalog service implementation, but also
 * to allow the implementor to have fine control over the actual API, URIs, and general JAX-RS annotations.
 *
 * @author Kelly Tisdell
 *
 */
@Transactional(readOnly = true)
@RestController
@RequestMapping(value = "/api/menu/",
    produces = {MediaType.APPLICATION_JSON_VALUE})
public class MenuEndPoint extends com.broadleafcommerce.rest.api.endpoint.page.PageEndpoint {
    @Resource(
        name = "blMenuProcessor"
    )
    protected MenuProcessor menuProcessor;

    @Resource(
        name = "blMenuService"
    )
    protected MenuService menuService;

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public List<MenuItemDTO>  findAllMenus() {
        Menu menu = this.menuService.findMenuByName("Header Nav");
        return this.menuService.constructMenuItemDTOsForMenu(menu);
    }

    @RequestMapping(value = "{navName}", method = RequestMethod.GET)
    public List<MenuItemDTO>  findAllMenus(@PathVariable("navName") String navName) {
        Menu menu = this.menuService.findMenuByName(navName);
        return this.menuService.constructMenuItemDTOsForMenu(menu);
    }
}
