package com.kapsulstore.ngsite.web.rest.checkout;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by mehmet on 7/13/17.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckoutAddress {
    public String name;
    public String surname;
    public String address1;
    public String address2;
    public String city;
    public String postalCode;
    public String phone;
    public String addressName;
    public String eMail;
}
