package com.kapsulstore.ngsite.web.rest.checkout;

import com.broadleafcommerce.rest.api.endpoint.checkout.CheckoutEndpoint;
import com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException;
import com.broadleafcommerce.rest.api.wrapper.*;
import com.iyzipay.model.CheckoutFormInitialize;
import com.kapsulstore.ngsite.service.payment.gateway.iyzico.IyzipayPaymentGatewayType;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.service.PaymentGatewayCheckoutService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.checkout.service.exception.CheckoutException;
import org.broadleafcommerce.core.checkout.service.workflow.CheckoutResponse;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.payment.domain.OrderPayment;
import org.broadleafcommerce.core.payment.domain.PaymentTransaction;
import org.broadleafcommerce.core.payment.domain.secure.Referenced;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mehmet on 7/11/17.
 */
@Transactional
@RestController
@RequestMapping(value = "/api/checkout",produces = { MediaType.APPLICATION_JSON_VALUE })
public class KSCheckoutEndPoint extends CheckoutEndpoint {
    @Resource(
        name = "blCustomerService"
    )
    protected CustomerService customerService;

    @Autowired(
        required = false
    )
    @Qualifier("blPaymentGatewayCheckoutService")
    protected PaymentGatewayCheckoutService paymentGatewayCheckoutService;

    @Override
    @RequestMapping(value = "/payments", method = RequestMethod.GET)
    public List<OrderPaymentWrapper> findPaymentsForOrder(HttpServletRequest request, Long cartId) {
        return super.findPaymentsForOrder(request, cartId);
    }

    @FrameworkMapping(
        value = {"/payment/{customerPaymentId}"},
        method = {RequestMethod.POST}
    )
    @RequestMapping(value = "/payment/{customerPaymentId}", method = RequestMethod.POST)
    @Override
    public OrderPaymentWrapper addPaymentToOrderById(HttpServletRequest request, Double amount, String currencyCode, Long customerPaymentId, Long cartId) {
        return super.addPaymentToOrderById(request, amount, currencyCode, customerPaymentId, cartId);
    }

    @FrameworkMapping(
        value = {"/payment"},
        method = {RequestMethod.POST},
        consumes = {"application/json"}
    )
    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    @Override
    public OrderPaymentWrapper addPaymentToOrder(HttpServletRequest request, @RequestBody OrderPaymentWrapper wrapper, @RequestParam("cartId") Long cartId) {
        OrderPaymentWrapper retVal = super.addPaymentToOrder(request, wrapper, cartId);
        return retVal;
    }



    @FrameworkMapping(
        value = {"/payment/{paymentId}"},
        method = {RequestMethod.DELETE}
    )
    @RequestMapping(value = "/payment/{paymentId}", method = RequestMethod.DELETE)
    @Override
    public OrderWrapper removePaymentFromOrderById(HttpServletRequest request, Long paymentId, Long cartId) {
        return super.removePaymentFromOrderById(request, paymentId, cartId);
    }

    @FrameworkMapping(
        value = {"/payment/{paymentId}/transaction"},
        method = {RequestMethod.PUT}
    )
    @RequestMapping(value = "/payment/{paymentId}/transaction", method = RequestMethod.PUT)
    @Override
    public OrderPaymentWrapper addOrderPaymentTransaction(HttpServletRequest request, Long orderPaymentId, PaymentTransactionWrapper wrapper, Long cartId) {


        Order cart = this.orderService.findOrderById(cartId);
        if(cart == null) {
            throw BroadleafWebServicesException.build(404).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.cartNotFound");
        } else {
            PaymentTransaction transaction = wrapper.unwrap(request, this.context);
            OrderPayment orderPayment = this.orderPaymentService.readPaymentById(orderPaymentId);
            if(orderPayment == null) {
                throw BroadleafWebServicesException.build(404).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.orderPaymentNotFound");
            } else if(orderPayment.getOrder() != null && orderPayment.getOrder().getId().equals(cartId)) {
                orderPayment.addTransaction(transaction);
                orderPayment = this.orderPaymentService.save(orderPayment);

                try {
                    this.orderService.save(cart, Boolean.valueOf(false));
                } catch (PricingException var9) {
                    ;
                }

                OrderPaymentWrapper paymentWrapper = (OrderPaymentWrapper)this.context.getBean(OrderPaymentWrapper.class.getName());
                paymentWrapper.wrapDetails(orderPayment, request);
                return paymentWrapper;
            } else {
                throw BroadleafWebServicesException.build(400).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.invalidOrderPaymentForOrder");
            }
        }
        //return super.addOrderPaymentTransaction(request, orderPaymentId, wrapper, cartId);
    }

    @FrameworkMapping(
        method = {RequestMethod.POST}
    )
    @RequestMapping(method = RequestMethod.POST)
    @Override
    public OrderWrapper performCheckout(HttpServletRequest request, @RequestParam("cartId") Long cartId) {
        return super.performCheckout(request, cartId);
    }

    @RequestMapping(value = "/anonymousCheckout", method = RequestMethod.POST)
    public OrderWrapper performAnonymousCheckout(HttpServletRequest request, @RequestParam("cartId") Long cartId, @RequestBody AnonymousCheckoutModel checkoutModel) {
        Order cart = this.orderService.findOrderById(cartId);
        try {
            this.processPassthroughCheckout(cart,PaymentType.CREDIT_CARD);
            CheckoutResponse response = this.checkoutService.performCheckout(cart);
            System.out.println(response.getOrder().getStatus().getType());
        } catch (PaymentException e) {
            e.printStackTrace();
        } catch (PricingException e) {
            e.printStackTrace();
        } catch (CheckoutException e) {
            e.printStackTrace();
        }
        if(true){
            return null;
        }

        if(cart != null) {
            try {




                Customer customer = customerService.createCustomer();
                customer.setEmailAddress(checkoutModel.customerEMail);
                customer.setFirstName(checkoutModel.billingAddress.name);
                customer.setLastName(checkoutModel.billingAddress.surname);
                customerService.saveCustomer(customer);
                OrderPaymentWrapper orderPaymentWrapper = new OrderPaymentWrapper();
                orderPaymentWrapper.setAmount(cart.getTotal().getAmount());
                orderPaymentWrapper.setCurrency(cart.getTotal().getCurrency());
                orderPaymentWrapper.setGatewayType("IYZICO_HOSTED_GATEWAY");
                orderPaymentWrapper.setType("CREDIT_CARD");
                AddressWrapper billingAddress = new AddressWrapper();
                billingAddress.setAddressLine1(checkoutModel.billingAddress.address1);
                billingAddress.setAddressLine2(checkoutModel.billingAddress.address2);
                billingAddress.setCity(checkoutModel.billingAddress.city);
                CountryWrapper country = new CountryWrapper();
                country.setName("Turkey");
                country.setAbbreviation("TR");
                billingAddress.setCountry(country);
                billingAddress.setFirstName(checkoutModel.billingAddress.name);
                billingAddress.setLastName(checkoutModel.billingAddress.surname);
                PhoneWrapper phone = new PhoneWrapper();
                phone.setPhoneNumber(checkoutModel.billingAddress.phone);
                billingAddress.setPhonePrimary( phone );
                billingAddress.setPostalCode(checkoutModel.billingAddress.postalCode);
                orderPaymentWrapper.setBillingAddress(billingAddress);
                PaymentTransactionWrapper transactionWrapper = (PaymentTransactionWrapper)this.context.getBean(PaymentTransactionWrapper.class.getName());
                OrderPaymentWrapper addPaymentToOrder = this.addPaymentToOrder(request, orderPaymentWrapper, cart.getId());
                PaymentTransactionWrapper paymentTransactionWrapper = (PaymentTransactionWrapper)this.context.getBean(PaymentTransactionWrapper.class.getName());

                //this.addOrderPaymentTransaction(request, addPaymentToOrder.getId(), paymentTransactionWrapper , cart.getId());

                cart = this.orderService.findOrderById(cartId);
                CheckoutResponse response = this.checkoutService.performCheckout(cart);
                Order order = response.getOrder();
                OrderWrapper wrapper = (OrderWrapper)this.context.getBean(OrderWrapper.class.getName());
                wrapper.wrapDetails(order, request);
                return wrapper;
            } catch (CheckoutException var7) {
                throw BroadleafWebServicesException.build(500).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.checkoutProcessingError");
            }
        } else {
            throw BroadleafWebServicesException.build(404).addMessage("com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.cartNotFound");
        }

    }
/*
    @RequestMapping(value = "/iyzicoPaymentForm", method = RequestMethod.GET)
    public CheckoutFormInitialize iyzicoPaymentForm(HttpServletRequest request) {
        return IyzicoBuilder.checkoutFormInitialize();
    }*/




    public String processPassthroughCheckout(Order cart, PaymentType paymentType) throws PaymentException, PricingException {
        //Order cart = CartState.getCart();
        List<OrderPayment> paymentsToInvalidate = new ArrayList();
        Iterator var5 = cart.getPayments().iterator();

        while(true) {
            while(true) {
                OrderPayment payment;
                do {
                    if(!var5.hasNext()) {
                        var5 = paymentsToInvalidate.iterator();

                        while(var5.hasNext()) {
                            payment = (OrderPayment)var5.next();
                            cart.getPayments().remove(payment);
                            if(this.paymentGatewayCheckoutService != null) {
                                this.paymentGatewayCheckoutService.markPaymentAsInvalid(payment.getId());
                            }
                        }

                        OrderPayment passthroughPayment = this.orderPaymentService.create();
                        passthroughPayment.setType(paymentType);
                        //passthroughPayment.setPaymentGatewayType(PaymentGatewayType.PASSTHROUGH);
                        passthroughPayment.setPaymentGatewayType(IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY);

                        passthroughPayment.setAmount(cart.getTotalAfterAppliedPayments());
                        passthroughPayment.setOrder(cart);
                        PaymentTransaction transaction = this.orderPaymentService.createTransaction();
                        transaction.setAmount(cart.getTotalAfterAppliedPayments());
                        transaction.setRawResponse("Passthrough Payment");
                        transaction.setSuccess(Boolean.valueOf(true));
                        transaction.setType(PaymentTransactionType.AUTHORIZE_AND_CAPTURE);
                        transaction.getAdditionalFields().put("IYZICO_PAYMENT_TYPE", paymentType.getType());
                        transaction.setOrderPayment(passthroughPayment);
                        passthroughPayment.addTransaction(transaction);
                        this.orderService.addPaymentToOrder(cart, passthroughPayment, (Referenced)null);
                        this.orderService.save(cart, Boolean.valueOf(true));
                        return "this.processCompleteCheckoutOrderFinalized(redirectAttributes);";
                    }

                    payment = (OrderPayment)var5.next();
                } while(!payment.isActive());

                if(payment.getTransactions() != null && !payment.getTransactions().isEmpty()) {
                    Iterator var7 = payment.getTransactions().iterator();

                    while(var7.hasNext()) {
                        PaymentTransaction transaction = (PaymentTransaction)var7.next();
                        if(!PaymentTransactionType.UNCONFIRMED.equals(transaction.getType())) {
                            paymentsToInvalidate.add(payment);
                        }
                    }
                } else {
                    paymentsToInvalidate.add(payment);
                }
            }
        }
    }


}
