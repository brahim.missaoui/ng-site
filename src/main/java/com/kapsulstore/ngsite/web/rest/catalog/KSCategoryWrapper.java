package com.kapsulstore.ngsite.web.rest.catalog;

import com.broadleafcommerce.rest.api.wrapper.CategoriesWrapper;
import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mehmet on 5/28/17.
 */
@Service("com.kapsulstore.ngsite.web.rest.catalog.KSCategoryWrapper")
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class KSCategoryWrapper extends CategoryWrapper {

}
