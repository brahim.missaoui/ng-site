package com.kapsulstore.ngsite.web.rest.customer;

import com.broadleafcommerce.rest.api.wrapper.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.broadleafcommerce.common.web.controller.annotation.FrameworkMapping;
import org.broadleafcommerce.core.web.controller.account.ChangePasswordForm;
import org.broadleafcommerce.profile.core.domain.*;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by mehmet on 7/11/17.
 */
@Transactional
@RestController
@RequestMapping(value = "/api/customer",produces = { MediaType.APPLICATION_JSON_VALUE })
public class KSCustomerEndpoint extends com.broadleafcommerce.rest.api.endpoint.customer.CustomerEndpoint{
    public KSCustomerEndpoint() {
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerEmailNotFound",
        response = ErrorWrapper.class
    ), @ApiResponse(
        code = 200,
        message = "OK"
    )})
    @FrameworkMapping(
        method = {RequestMethod.GET}
    )
    @RequestMapping(method = RequestMethod.GET)
    public CustomerWrapper findCustomerByEmail(HttpServletRequest request, @RequestParam(value = "email",required = true) String emailAddress) {
        return super.findCustomerByEmail(request, emailAddress);
    }

    @FrameworkMapping(
        method = {RequestMethod.POST},
        consumes = {"application/json", "application/xml"}
    )
    @RequestMapping(method = RequestMethod.POST)
    public CustomerWrapper addCustomer(HttpServletRequest request, @RequestBody CustomerWrapper wrapper) {
        return super.addCustomer(request, wrapper);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        method = {RequestMethod.PUT},
        consumes = {"application/json", "application/xml"}
    )
    @RequestMapping(method = RequestMethod.PUT)
    public CustomerWrapper updateCustomer(HttpServletRequest request, @RequestBody CustomerWrapper wrapper) {
        return super.updateCustomer(request, wrapper);
    }

    @FrameworkMapping(
        method = {RequestMethod.POST},
        value = {"/password"}
    )
    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public CustomerWrapper changePassword(HttpServletRequest request, @RequestBody ChangePasswordForm changePasswordForm, BindingResult result) {
        return super.changePassword(request, changePasswordForm, result);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/attributes"},
        method = {RequestMethod.DELETE}
    )
    @RequestMapping(value = "/attributes", method = RequestMethod.DELETE)
    public CustomerWrapper removeAllAttributes(HttpServletRequest request) {
        return super.removeAllAttributes(request, (Long)null);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/attribute"},
        method = {RequestMethod.PUT},
        consumes = {"application/json", "application/xml"}
    )
    @RequestMapping(value = "/attribute", method = RequestMethod.PUT)
    public CustomerWrapper addAttribute(HttpServletRequest request, @RequestBody CustomerAttributeWrapper wrapper) {
        return super.addAttribute(request, (Long)null, wrapper);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/attribute/{attributeName}"},
        method = {RequestMethod.DELETE}
    )
    @RequestMapping(value = "/attribute/{attributeName}", method = RequestMethod.DELETE)
    public CustomerWrapper removeAttribute(HttpServletRequest request, @PathVariable("attributeName") String attributeName) {
        return super.removeAttribute(request, (Long)null, attributeName);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    ), @ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerAddressNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/address"},
        method = {RequestMethod.GET}
    )
    @RequestMapping(value = "/address", method = RequestMethod.GET)
    public CustomerAddressWrapper findAddress(HttpServletRequest request, @RequestParam("addressName") String addressName) {
        return super.findAddress(request, (Long)null, addressName);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/addresses"},
        method = {RequestMethod.GET}
    )
    @RequestMapping(value = "/addresses", method = RequestMethod.GET)
    public List<CustomerAddressWrapper> findAllAddresses(HttpServletRequest request) {
        return super.findAllAddresses(request, (Long)null);
    }


    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/address"},
        method = {RequestMethod.PUT}
    )
    @RequestMapping(value = "/address", method = RequestMethod.PUT)
    public CustomerAddressWrapper addAddress(HttpServletRequest request, @RequestBody CustomerAddressWrapper wrapper) {
        return super.addAddress(request, (Long)null, wrapper);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/address/{addressId}"},
        method = {RequestMethod.PUT}
    )
    @RequestMapping(value = "/address/{addressId}", method = RequestMethod.PUT)
    public CustomerAddressWrapper updateAddress(HttpServletRequest request, @PathVariable("addressId") Long customerAddressId, @RequestBody CustomerAddressWrapper wrapper) {
        return super.updateAddress(request, (Long)null, customerAddressId, wrapper);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/addresses"},
        method = {RequestMethod.DELETE}
    )
    @RequestMapping(value = "/addresses", method = RequestMethod.DELETE)
    public CustomerWrapper removeAllAddresses(HttpServletRequest request) {
        return super.removeAllAddresses(request, (Long)null);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/address/{addressName}"},
        method = {RequestMethod.DELETE}
    )
    @RequestMapping(value = "/address/{addressName}", method = RequestMethod.DELETE)
    public List<CustomerAddressWrapper> removeAddress(HttpServletRequest request, @PathVariable("addressName") String addressName) {
        return super.removeAddress(request, (Long)null, addressName);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/payment"},
        method = {RequestMethod.GET}
    )
    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public CustomerPaymentWrapper findCustomerPayment(HttpServletRequest request, @RequestParam("paymentId") Long paymentId) {
        return super.findCustomerPayment(request, (Long)null, paymentId);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/payments"},
        method = {RequestMethod.GET}
    )
    @RequestMapping(value = "/payments", method = RequestMethod.GET)
    public List<CustomerPaymentWrapper> findAllCustomerPayments(HttpServletRequest request) {
        return super.findAllCustomerPayments(request, (Long)null);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/payment"},
        method = {RequestMethod.POST}
    )
    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public CustomerPaymentWrapper addCustomerPayment(HttpServletRequest request, @RequestBody CustomerPaymentWrapper wrapper) {
        return super.addCustomerPayment(request, (Long)null, wrapper);
    }

    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/payments"},
        method = {RequestMethod.DELETE}
    )
    //@RequestMapping(value = "/payments", method = RequestMethod.DELETE)
    public CustomerWrapper removeAllCustomerPayments(HttpServletRequest request) {
        return super.removeAllCustomerPayments(request, (Long)null);
    }


    @ApiResponses({@ApiResponse(
        code = 404,
        message = "com.broadleafcommerce.rest.api.exception.BroadleafWebServicesException.customerNotFound",
        response = ErrorWrapper.class
    )})
    @ApiImplicitParams({@ApiImplicitParam(
        name = "customerId",
        paramType = "query",
        dataType = "long",
        required = true
    )})
    @FrameworkMapping(
        value = {"/payment/{paymentId}"},
        method = {RequestMethod.DELETE}
    )
    public List<CustomerPaymentWrapper> removeCustomerPayment(HttpServletRequest request, @PathVariable("paymentId") Long paymentId) {
        return super.removeCustomerPayment(request, (Long)null, paymentId);
    }

    protected Customer getCustomer(Long customerId) {
        return super.getCustomer(customerId);
    }
}
