package com.kapsulstore.ngsite.web.rest.catalog;

import com.broadleafcommerce.rest.api.wrapper.CategoriesWrapper;
import com.broadleafcommerce.rest.api.wrapper.CategoryWrapper;
import org.broadleafcommerce.core.catalog.domain.Category;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Target;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mehmet on 5/28/17.
 */
@Service("com.kapsulstore.ngsite.web.rest.catalog.KSCategoriesWrapper")
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class KSCategoriesWrapper extends CategoriesWrapper {

    @Override
    public void wrapDetails(List<Category> cats, HttpServletRequest request) {
        Iterator var3 = cats.iterator();

        while(var3.hasNext()) {
            Category category = (Category)var3.next();
            CategoryWrapper wrapper = (CategoryWrapper)this.context.getBean(CategoryWrapper.class.getName());
            wrapper.wrapDetails(category, request);
            this.categories.add(wrapper);
        }
    }
    @Override
    public List<CategoryWrapper> getCategories() {
        return this.categories;
    }
}
