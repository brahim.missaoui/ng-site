package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.PaymentGatewayConfiguration;
import org.broadleafcommerce.common.payment.service.PaymentGatewayWebResponseService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.common.web.payment.controller.PaymentGatewayAbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by mehmet on 7/14/17.
 */
@Controller("IyzipaySamplePaymentGatewayHostedController")
@RequestMapping({"/sample-checkout"})
public class IyzipaySamplePaymentGatewayHostedController extends PaymentGatewayAbstractController{
    protected static final Log LOG = LogFactory.getLog(org.broadleafcommerce.vendor.sample.web.controller.SamplePaymentGatewayHostedController.class);
    protected static final String GATEWAY_CONTEXT_KEY = "sample-checkout";
    @Resource(name = "blSamplePaymentGatewayHostedWebResponseService")
    protected PaymentGatewayWebResponseService paymentGatewayWebResponseService;
    @Resource(name = "blSamplePaymentGatewayHostedConfiguration")
    protected PaymentGatewayConfiguration paymentGatewayConfiguration;

    public IyzipaySamplePaymentGatewayHostedController(){}

    public void handleProcessingException(Exception e, RedirectAttributes redirectAttributes) throws PaymentException {
        if(LOG.isTraceEnabled()) {
            LOG.trace("A Processing Exception Occurred for sample-checkout. Adding Error to Redirect Attributes.");
        }

        redirectAttributes.addAttribute("PAYMENT_PROCESSING_ERROR", getProcessingErrorMessage());
    }

    public void handleUnsuccessfulTransaction(Model model, RedirectAttributes redirectAttributes, PaymentResponseDTO responseDTO) throws PaymentException {
        if(LOG.isTraceEnabled()) {
            LOG.trace("The Transaction was unsuccessful for sample-checkout. Adding Errors to Redirect Attributes.");
        }

        redirectAttributes.addAttribute("PAYMENT_PROCESSING_ERROR", responseDTO.getResponseMap().get("MESSAGE"));
    }

    public String getGatewayContextKey() {
        return "sample-checkout";
    }

    public PaymentGatewayWebResponseService getWebResponseService() {
        return this.paymentGatewayWebResponseService;
    }

    public PaymentGatewayConfiguration getConfiguration() {
        return this.paymentGatewayConfiguration;
    }

    @RequestMapping(
        value = {"/hosted/return"},
        method = {RequestMethod.GET}
    )
    public String returnEndpoint(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, Map<String, String> pathVars) throws PaymentException {
        return super.process(model, request, redirectAttributes);
    }

    @RequestMapping(
        value = {"/hosted/error"},
        method = {RequestMethod.GET}
    )
    public String errorEndpoint(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, Map<String, String> pathVars) throws PaymentException {
        redirectAttributes.addAttribute("PAYMENT_PROCESSING_ERROR", request.getParameter("PAYMENT_PROCESSING_ERROR"));
        return this.getOrderReviewRedirect();
    }
}
