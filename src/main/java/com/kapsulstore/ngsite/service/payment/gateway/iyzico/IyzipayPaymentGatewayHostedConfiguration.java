package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.service.PaymentGatewayConfiguration;

/**
 * Created by mehmet on 7/12/17.
 */
public interface IyzipayPaymentGatewayHostedConfiguration extends PaymentGatewayConfiguration {
    String getHostedRedirectUrl();

    String getHostedRedirectReturnUrl();
}
