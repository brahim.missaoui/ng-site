package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.PaymentGatewayType;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayConfiguration;
import org.springframework.stereotype.Service;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedConfiguration")
public class IyzipayPaymentGatewayHostedConfigurationImpl extends AbstractPaymentGatewayConfiguration implements IyzipayPaymentGatewayHostedConfiguration {
    protected int failureReportingThreshold = 1;
    protected boolean performAuthorizeAndCapture = true;

    public IyzipayPaymentGatewayHostedConfigurationImpl() {
    }

    public String getHostedRedirectUrl() {
        return "/hosted/sample-checkout";
    }

    public String getHostedRedirectReturnUrl() {
        return "/sample-checkout/hosted/return";
    }

    public boolean isPerformAuthorizeAndCapture() {
        return true;
    }

    public void setPerformAuthorizeAndCapture(boolean performAuthorizeAndCapture) {
        this.performAuthorizeAndCapture = performAuthorizeAndCapture;
    }

    public int getFailureReportingThreshold() {
        return this.failureReportingThreshold;
    }

    public void setFailureReportingThreshold(int failureReportingThreshold) {
        this.failureReportingThreshold = failureReportingThreshold;
    }

    public boolean handlesAuthorize() {
        return true;
    }

    public boolean handlesCapture() {
        return false;
    }

    public boolean handlesAuthorizeAndCapture() {
        return true;
    }

    public boolean handlesReverseAuthorize() {
        return false;
    }

    public boolean handlesVoid() {
        return false;
    }

    public boolean handlesRefund() {
        return false;
    }

    public boolean handlesPartialCapture() {
        return false;
    }

    public boolean handlesMultipleShipment() {
        return false;
    }

    public boolean handlesRecurringPayment() {
        return false;
    }

    public boolean handlesSavedCustomerPayment() {
        return false;
    }

    public boolean handlesMultiplePayments() {
        return false;
    }

    public PaymentGatewayType getGatewayType() {
        return IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY;
    }
}
