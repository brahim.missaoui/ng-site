package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import com.iyzipay.model.Locale;
import com.iyzipay.request.RetrieveCheckoutFormRequest;
import com.kapsulstore.ngsite.web.rest.checkout.IyzicoBuilder;
import org.broadleafcommerce.common.currency.domain.BroadleafCurrency;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayWebResponseService;
import org.broadleafcommerce.common.payment.service.PaymentGatewayWebResponsePrintService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Currency;
import java.util.Map;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedWebResponseService")
public class IyzipayPaymentGatewayHostedWebResponseServiceImpl extends AbstractPaymentGatewayWebResponseService {
    @Resource(name = "blPaymentGatewayWebResponsePrintService")
    protected PaymentGatewayWebResponsePrintService webResponsePrintService;

    public IyzipayPaymentGatewayHostedWebResponseServiceImpl() {
    }

    public PaymentResponseDTO translateWebResponse(HttpServletRequest request) throws PaymentException {
        PaymentResponseDTO responseDTO = (new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY)).rawResponse(this.webResponsePrintService.printRequest(request));
        Map<String, String[]> paramMap = request.getParameterMap();
        String token = paramMap.get("token")[0];
        System.out.println("transactionCallback:" + token);
        RetrieveCheckoutFormRequest irequest = new RetrieveCheckoutFormRequest();
        irequest.setLocale(Locale.TR.getValue());
        irequest.setToken(token);
        com.iyzipay.model.CheckoutForm checkoutForm = com.iyzipay.model.CheckoutForm.retrieve(irequest, IyzicoBuilder.getOptions());

        responseDTO.responseMap("TOKEN", token);
        responseDTO.responseMap("MESSAGE", checkoutForm.getErrorMessage());
        responseDTO.rawResponse(this.webResponsePrintService.printRequest(request));
        responseDTO.successful(checkoutForm.getStatus().equals("success"));

        responseDTO.paymentTransactionType(PaymentTransactionType.AUTHORIZE_AND_CAPTURE);

        /*responseDTO.successful(true);
        String[] complete_checkout_on_callbacks = paramMap.get("COMPLETE_CHECKOUT_ON_CALLBACK");
        boolean completeCheckoutOnCallback = Boolean.parseBoolean(((String[]) complete_checkout_on_callbacks )[0]);
        responseDTO.completeCheckoutOnCallback(completeCheckoutOnCallback);
        responseDTO.amount(amount);
        responseDTO.paymentTransactionType(PaymentTransactionType.UNCONFIRMED);
        responseDTO.orderId(((String[])paramMap.get("ORDER_ID"))[0]);
        responseDTO.responseMap("MESSAGE", ((String[])paramMap.get("MESSAGE"))[0]);*/
        return responseDTO;
    }
}
