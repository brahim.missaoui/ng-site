package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayRollbackService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.springframework.stereotype.Service;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedRollbackService")
public class IyzipayPaymentGatewayHostedRollbackServiceImpl extends AbstractPaymentGatewayRollbackService {
    protected static final Log LOG = LogFactory.getLog(IyzipayPaymentGatewayHostedRollbackServiceImpl.class);

    public IyzipayPaymentGatewayHostedRollbackServiceImpl() {
    }

    public PaymentResponseDTO rollbackAuthorize(PaymentRequestDTO transactionToBeRolledBack) throws PaymentException {
        if(LOG.isTraceEnabled()) {
            LOG.trace("Iyzico Payment Hosted Gateway - Rolling back authorize transaction with amount: " + transactionToBeRolledBack.getTransactionTotal());
        }

        return (new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY)).rawResponse("rollback authorize - successful").successful(true).paymentTransactionType(PaymentTransactionType.REVERSE_AUTH).amount(new Money(transactionToBeRolledBack.getTransactionTotal()));
    }

    public PaymentResponseDTO rollbackCapture(PaymentRequestDTO transactionToBeRolledBack) throws PaymentException {
        throw new PaymentException("The Rollback Capture method is not supported for this module");
    }

    public PaymentResponseDTO rollbackAuthorizeAndCapture(PaymentRequestDTO transactionToBeRolledBack) throws PaymentException {
        if(LOG.isTraceEnabled()) {
            LOG.trace("Iyzico Payment Hosted Gateway - Rolling back authorize and capture transaction with amount: " + transactionToBeRolledBack.getTransactionTotal());
        }

        return (new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY)).rawResponse("rollback authorize and capture - successful").successful(true).paymentTransactionType(PaymentTransactionType.VOID).amount(new Money(transactionToBeRolledBack.getTransactionTotal()));
    }

    public PaymentResponseDTO rollbackRefund(PaymentRequestDTO transactionToBeRolledBack) throws PaymentException {
        throw new PaymentException("The Rollback Refund method is not supported for this module");
    }
}
