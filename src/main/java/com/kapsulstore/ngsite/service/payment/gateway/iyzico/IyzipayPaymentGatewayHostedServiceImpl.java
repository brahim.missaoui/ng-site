package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import com.iyzipay.model.*;
import com.iyzipay.request.CreateCheckoutFormInitializeRequest;
import com.kapsulstore.ngsite.web.rest.checkout.IyzicoBuilder;
import com.kapsulstore.ngsite.web.rest.checkout.KSIyzipayCheckoutFormInitialize;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.LineItemDTO;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayHostedService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedService")
public class IyzipayPaymentGatewayHostedServiceImpl extends AbstractPaymentGatewayHostedService {
    @Resource(
        name = "iyzipayPaymentGatewayHostedConfiguration"
    )
    protected IyzipayPaymentGatewayHostedConfiguration configuration;

    public IyzipayPaymentGatewayHostedServiceImpl() {
    }

    public PaymentResponseDTO requestHostedEndpoint(PaymentRequestDTO requestDTO) throws PaymentException {

        CreateCheckoutFormInitializeRequest request = new CreateCheckoutFormInitializeRequest();


        request.setLocale(Locale.TR.getValue());
        request.setConversationId(requestDTO.getOrderId());

        request.setPrice(new BigDecimal(requestDTO.getOrderSubtotal()));
        request.setPaidPrice(new BigDecimal(requestDTO.getOrderSubtotal()));
        request.setCurrency(Currency.TRY.name());
        request.setBasketId(requestDTO.getOrderId());
        request.setPaymentGroup(PaymentGroup.PRODUCT.name());
        request.setCallbackUrl(IyzicoBuilder.callBackHostname() + "/api/iyzipay/transaction/callback");

        List<Integer> enabledInstallments = new ArrayList<Integer>();
        enabledInstallments.add(1);
        enabledInstallments.add(2);
        enabledInstallments.add(3);
        enabledInstallments.add(4);
        request.setEnabledInstallments(enabledInstallments);

        Buyer buyer = new Buyer();
        buyer.setId(requestDTO.getCustomer().getCustomerId());
        buyer.setName(requestDTO.getCustomer().getFirstName());
        buyer.setSurname(requestDTO.getCustomer().getLastName());
        buyer.setGsmNumber(requestDTO.getCustomer().getPhone());
        buyer.setEmail(requestDTO.getCustomer().getEmail());
        buyer.setIdentityNumber("74300864791");
        buyer.setLastLoginDate("2015-10-05 12:43:35");
        buyer.setRegistrationDate("2013-04-21 15:12:09");
        buyer.setRegistrationAddress(requestDTO.shipTo().getAddressLine1() + requestDTO.shipTo().getAddressLine2());
        buyer.setIp("85.34.78.112");
        buyer.setCity("Istanbul");
        buyer.setCountry("Turkey");
        buyer.setZipCode(requestDTO.billTo().getAddressPostalCode());
        request.setBuyer(buyer);

        Address shippingAddress = new Address();
        shippingAddress.setContactName(requestDTO.shipTo().getAddressFirstName() + " " + requestDTO.shipTo().getAddressLastName());
        shippingAddress.setCity("Istanbul");
        shippingAddress.setCountry("Turkey");
        shippingAddress.setAddress(requestDTO.shipTo().getAddressLine1() + " " + requestDTO.shipTo().getAddressLine2());
        shippingAddress.setZipCode(requestDTO.shipTo().getAddressPostalCode());
        request.setShippingAddress(shippingAddress);

        Address billingAddress = new Address();
        billingAddress.setContactName(requestDTO.billTo().getAddressFirstName() +" "+ requestDTO.billTo().getAddressLastName());
        billingAddress.setCity("Istanbul");
        billingAddress.setCountry("Turkey");
        billingAddress.setAddress(requestDTO.billTo().getAddressLine1() +" "+ requestDTO.billTo().getAddressLine2());
        billingAddress.setZipCode(requestDTO.billTo().getAddressPostalCode());
        request.setBillingAddress(billingAddress);

        List<BasketItem> basketItems = new ArrayList<BasketItem>();

        for (LineItemDTO itemDTO: requestDTO.getLineItems()) {
            BasketItem basketItem = new BasketItem();
            basketItem.setId(itemDTO.getSystemId());
            basketItem.setName(itemDTO.getName());
            basketItem.setCategory1(itemDTO.getCategory());
            basketItem.setCategory2("");
            basketItem.setItemType(BasketItemType.PHYSICAL.name());
            basketItem.setPrice(new BigDecimal(itemDTO.getItemTotal()));
            basketItems.add(basketItem);
        }

        request.setBasketItems(basketItems);


        CheckoutFormInitialize checkoutFormInitialize = KSIyzipayCheckoutFormInitialize.create(request, IyzicoBuilder.getOptions());
        Map<String, String> paymentFormResultMap = IyzicoBuilder.paymentFormResultToMap(checkoutFormInitialize);
        System.out.println(paymentFormResultMap);

        PaymentResponseDTO paymentResponseDTO = new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY);
        paymentResponseDTO.completeCheckoutOnCallback(requestDTO.isCompleteCheckoutOnCallback());
        paymentResponseDTO.responseMap("ORDER_ID", requestDTO.getOrderId());
        paymentResponseDTO.responseMap("TRANSACTION_AMT", requestDTO.getTransactionTotal());
        paymentResponseDTO.responseMap("HOSTED_REDIRECT_URL", checkoutFormInitialize.getPaymentPageUrl());
        paymentResponseDTO.responseMap("COMPLETE_CHECKOUT_ON_CALLBACK", "true");
        paymentResponseDTO.responseMap("TOKEN", checkoutFormInitialize.getToken());
        paymentResponseDTO.getResponseMap().putAll(paymentFormResultMap);



        //PaymentResponseDTO responseDTO = (new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY)).completeCheckoutOnCallback(requestDTO.isCompleteCheckoutOnCallback()).responseMap("ORDER_ID", requestDTO.getOrderId()).responseMap("TRANSACTION_AMT", requestDTO.getTransactionTotal()).responseMap("HOSTED_REDIRECT_URL", this.configuration.getHostedRedirectUrl())
        return paymentResponseDTO;
    }
}
