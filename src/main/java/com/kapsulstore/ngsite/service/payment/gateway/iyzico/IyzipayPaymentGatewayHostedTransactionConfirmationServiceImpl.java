package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayTransactionConfirmationService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedTransactionConfirmationService")
public class IyzipayPaymentGatewayHostedTransactionConfirmationServiceImpl extends AbstractPaymentGatewayTransactionConfirmationService {
    protected static final Log LOG = LogFactory.getLog(IyzipayPaymentGatewayHostedTransactionConfirmationServiceImpl.class);
    @Resource(
        name = "iyzipayPaymentGatewayHostedConfiguration"
    )
    protected IyzipayPaymentGatewayHostedConfiguration configuration;

    public IyzipayPaymentGatewayHostedTransactionConfirmationServiceImpl() {
    }

    public PaymentResponseDTO confirmTransaction(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        if(LOG.isTraceEnabled()) {
            LOG.trace("Iyzico Payment Hosted Gateway - Confirming Transaction with amount: " + paymentRequestDTO.getTransactionTotal());
        }

        PaymentTransactionType type = PaymentTransactionType.AUTHORIZE_AND_CAPTURE;
        if(!this.configuration.isPerformAuthorizeAndCapture()) {
            type = PaymentTransactionType.AUTHORIZE;
        }

        return (new PaymentResponseDTO(PaymentType.THIRD_PARTY_ACCOUNT, IyzipayPaymentGatewayType.IYZICO_HOSTED_GATEWAY)).rawResponse("confirmation - successful").successful(true).paymentTransactionType(type).amount(new Money(paymentRequestDTO.getTransactionTotal()));
    }
}
