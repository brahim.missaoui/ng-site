package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.service.*;
import org.broadleafcommerce.common.web.payment.expression.PaymentGatewayFieldExtensionHandler;
import org.broadleafcommerce.common.web.payment.processor.CreditCardTypesExtensionHandler;
import org.broadleafcommerce.common.web.payment.processor.TRCreditCardExtensionHandler;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayConfigurationService")
public class IyzipayPaymentGatewayConfigurationServiceImpl extends AbstractPaymentGatewayConfigurationService {
    @Resource(
        name = "iyzipayPaymentGatewayConfiguration"
    )
    protected IyzipayPaymentGatewayConfiguration configuration;
    @Resource(
        name = "iyzipayPaymentGatewayTransactionService"
    )
    protected PaymentGatewayTransactionService transactionService;
    @Resource(
        name = "iyzipayPaymentGatewayRollbackService"
    )
    protected PaymentGatewayRollbackService rollbackService;
    @Resource(
        name = "iyzipayPaymentGatewayWebResponseService"
    )
    protected PaymentGatewayWebResponseService webResponseService;
    @Resource(
        name = "iyzipayPaymentGatewayTransparentRedirectService"
    )
    protected PaymentGatewayTransparentRedirectService transparentRedirectService;
    @Resource(
        name = "iyzipayPaymentGatewayTRExtensionHandler"
    )
    protected TRCreditCardExtensionHandler creditCardExtensionHandler;
    @Resource(
        name = "iyzipayPaymentGatewayFieldExtensionHandler"
    )
    protected PaymentGatewayFieldExtensionHandler fieldExtensionHandler;

    public IyzipayPaymentGatewayConfigurationServiceImpl() {
    }

    public PaymentGatewayConfiguration getConfiguration() {
        return this.configuration;
    }

    public PaymentGatewayTransactionService getTransactionService() {
        return this.transactionService;
    }

    public PaymentGatewayTransactionConfirmationService getTransactionConfirmationService() {
        return null;
    }

    public PaymentGatewayReportingService getReportingService() {
        return null;
    }

    public PaymentGatewayCreditCardService getCreditCardService() {
        return null;
    }

    public PaymentGatewayCustomerService getCustomerService() {
        return null;
    }

    public PaymentGatewaySubscriptionService getSubscriptionService() {
        return null;
    }

    public PaymentGatewayFraudService getFraudService() {
        return null;
    }

    public PaymentGatewayHostedService getHostedService() {
        return null;
    }

    public PaymentGatewayRollbackService getRollbackService() {
        return this.rollbackService;
    }

    public PaymentGatewayWebResponseService getWebResponseService() {
        return this.webResponseService;
    }

    public PaymentGatewayTransparentRedirectService getTransparentRedirectService() {
        return this.transparentRedirectService;
    }

    public TRCreditCardExtensionHandler getCreditCardExtensionHandler() {
        return this.creditCardExtensionHandler;
    }

    public PaymentGatewayFieldExtensionHandler getFieldExtensionHandler() {
        return this.fieldExtensionHandler;
    }

    public CreditCardTypesExtensionHandler getCreditCardTypesExtensionHandler() {
        return null;
    }
}
