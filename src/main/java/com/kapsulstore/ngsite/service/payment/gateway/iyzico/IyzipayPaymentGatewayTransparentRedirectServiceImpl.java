package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.PaymentGatewayRequestType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.AddressDTO;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayTransparentRedirectService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayTransparentRedirectService")
public class IyzipayPaymentGatewayTransparentRedirectServiceImpl extends AbstractPaymentGatewayTransparentRedirectService {
    @Resource(
        name = "iyzipayPaymentGatewayConfiguration"
    )
    protected IyzipayPaymentGatewayConfiguration configuration;

    public IyzipayPaymentGatewayTransparentRedirectServiceImpl() {
    }

    public String getCreateCustomerPaymentTokenReturnURLFieldKey(PaymentResponseDTO responseDTO) {
        return "TRANSPARENT_REDIRECT_RETURN_URL";
    }

    public String getCreateCustomerPaymentTokenCancelURLFieldKey(PaymentResponseDTO responseDTO) {
        return "TRANSPARENT_REDIRECT_RETURN_URL";
    }

    public String getUpdateCustomerPaymentTokenReturnURLFieldKey(PaymentResponseDTO responseDTO) {
        return "TRANSPARENT_REDIRECT_RETURN_URL";
    }

    public String getUpdateCustomerPaymentTokenCancelURLFieldKey(PaymentResponseDTO responseDTO) {
        return "TRANSPARENT_REDIRECT_RETURN_URL";
    }

    public PaymentResponseDTO createAuthorizeForm(PaymentRequestDTO requestDTO) throws PaymentException {
        return this.createCommonTRFields(requestDTO);
    }

    public PaymentResponseDTO createAuthorizeAndCaptureForm(PaymentRequestDTO requestDTO) throws PaymentException {
        return this.createCommonTRFields(requestDTO);
    }

    public PaymentResponseDTO createCustomerPaymentTokenForm(PaymentRequestDTO requestDTO) throws PaymentException {
        return this.createCommonTRFields(requestDTO);
    }

    public PaymentResponseDTO updateCustomerPaymentTokenForm(PaymentRequestDTO requestDTO) throws PaymentException {
        return this.createCommonTRFields(requestDTO);
    }

    protected PaymentResponseDTO createCommonTRFields(PaymentRequestDTO requestDTO) {
        if(!PaymentGatewayRequestType.CREATE_CUSTOMER_PAYMENT_TR.equals(requestDTO.getGatewayRequestType()) && !PaymentGatewayRequestType.UPDATE_CUSTOMER_PAYMENT_TR.equals(requestDTO.getGatewayRequestType())) {
            Assert.isTrue(requestDTO.getTransactionTotal() != null, "The Transaction Total on the Payment Request DTO must not be null");
            Assert.isTrue(requestDTO.getOrderId() != null, "The Order ID on the Payment Request DTO must not be null");
        } else {
            Assert.isTrue(requestDTO.getCustomer() != null, "The Customer on the Payment Request DTO must not be null for a Customer Payment tokenization request.");
        }

        PaymentResponseDTO responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
        if(!PaymentGatewayRequestType.CREATE_CUSTOMER_PAYMENT_TR.equals(requestDTO.getGatewayRequestType()) && !PaymentGatewayRequestType.UPDATE_CUSTOMER_PAYMENT_TR.equals(requestDTO.getGatewayRequestType())) {
            responseDTO.responseMap("TRANSPARENT_REDIRECT_URL", this.configuration.getTransparentRedirectUrl());
            responseDTO.responseMap("TRANSPARENT_REDIRECT_RETURN_URL", this.configuration.getTransparentRedirectReturnUrl());
            responseDTO.responseMap("ORDER_ID", requestDTO.getOrderId());
            responseDTO.responseMap("TRANSACTION_AMT", requestDTO.getTransactionTotal());
        } else {
            responseDTO.responseMap("TRANSPARENT_REDIRECT_URL", this.configuration.getCustomerPaymentTransparentRedirectUrl());
            responseDTO.responseMap("TRANSPARENT_REDIRECT_RETURN_URL", this.configuration.getCustomerPaymentTransparentRedirectReturnUrl());
            responseDTO.responseMap("CUSTOMER_ID", requestDTO.getCustomer().getCustomerId());
        }

        AddressDTO billTo = requestDTO.getBillTo();
        if(billTo != null) {
            responseDTO.responseMap("NULL_BILLING_FIRST_NAME", billTo.getAddressFirstName()).responseMap("NULL_BILLING_LAST_NAME", billTo.getAddressLastName()).responseMap("NULL_BILLING_ADDRESS_LINE1", billTo.getAddressLine1()).responseMap("NULL_BILLING_ADDRESS_LINE2", billTo.getAddressLine2()).responseMap("NULL_BILLING_CITY", billTo.getAddressCityLocality()).responseMap("NULL_BILLING_STATE", billTo.getAddressStateRegion()).responseMap("NULL_BILLING_ZIP", billTo.getAddressPostalCode()).responseMap("NULL_BILLING_COUNTRY", billTo.getAddressCountryCode());
        }

        AddressDTO shipTo = requestDTO.getShipTo();
        if(shipTo != null) {
            responseDTO.responseMap("NULL_SHIPPING_FIRST_NAME", shipTo.getAddressFirstName()).responseMap("NULL_SHIPPING_LAST_NAME", shipTo.getAddressLastName()).responseMap("NULL_SHIPPING_ADDRESS_LINE1", shipTo.getAddressLine1()).responseMap("NULL_SHIPPING_ADDRESS_LINE2", shipTo.getAddressLine2()).responseMap("NULL_SHIPPING_CITY", shipTo.getAddressCityLocality()).responseMap("NULL_SHIPPING_STATE", shipTo.getAddressStateRegion()).responseMap("NULL_SHIPPING_ZIP", shipTo.getAddressPostalCode()).responseMap("NULL_SHIPPING_COUNTRY", shipTo.getAddressCountryCode());
        }

        return responseDTO;
    }
}
