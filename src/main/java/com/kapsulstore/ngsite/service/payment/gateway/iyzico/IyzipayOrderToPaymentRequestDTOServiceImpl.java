package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.common.payment.dto.LineItemDTO;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.domain.OrderItem;
import org.broadleafcommerce.core.payment.service.OrderToPaymentRequestDTOService;
import org.broadleafcommerce.core.payment.service.OrderToPaymentRequestDTOServiceImpl;
import org.springframework.stereotype.Service;

/**
 * Created by mehmet on 7/14/17.
 */
@Service("iyzipayOrderToPaymentRequestDTOService")
public class IyzipayOrderToPaymentRequestDTOServiceImpl extends OrderToPaymentRequestDTOServiceImpl {
    private static final Log LOG = LogFactory.getLog(IyzipayOrderToPaymentRequestDTOServiceImpl.class);

    @Override
    public PaymentRequestDTO translateOrder(Order order) {
        PaymentRequestDTO paymentRequestDTO = super.translateOrder(order);
        for (OrderItem orderItem: order.getOrderItems()) {
            LineItemDTO lineItem = paymentRequestDTO.lineItem();
            lineItem.systemId(orderItem.getId().toString());
            lineItem.name(orderItem.getName());
            lineItem.amount(orderItem.getSalePrice().getAmount().toString());
            lineItem.category("Giyim");
            lineItem.quantity(orderItem.getQuantity()+"");
            lineItem.itemTotal(orderItem.getSalePrice().getAmount().toString());
            paymentRequestDTO.getLineItems().add(lineItem);
        }
        return paymentRequestDTO;
    }
}
