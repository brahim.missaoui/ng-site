package com.kapsulstore.ngsite.service.payment.gateway.iyzico.expression;

import org.broadleafcommerce.common.payment.PaymentGatewayType;
import org.broadleafcommerce.common.payment.service.PaymentGatewayConfiguration;
import org.broadleafcommerce.common.web.payment.expression.AbstractPaymentGatewayFieldExtensionHandler;
import org.broadleafcommerce.common.web.payment.expression.PaymentGatewayFieldExtensionManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayFieldExtensionHandler")
public class IyzipayPaymentGatewayFieldExtensionHandler extends AbstractPaymentGatewayFieldExtensionHandler {
    @Resource(
        name = "blPaymentGatewayFieldExtensionManager"
    )
    protected PaymentGatewayFieldExtensionManager extensionManager;
    @Resource(
        name = "iyzipayPaymentGatewayConfiguration"
    )
    protected PaymentGatewayConfiguration paymentGatewayConfiguration;

    public IyzipayPaymentGatewayFieldExtensionHandler() {
    }

    @PostConstruct
    public void init() {
        if(this.isEnabled()) {
            this.extensionManager.registerHandler(this);
        }

    }

    public String getCreditCardHolderName() {
        return "CREDIT_CARD_NAME";
    }

    public String getCreditCardType() {
        return null;
    }

    public String getCreditCardNum() {
        return "CREDIT_CARD_NUMBER";
    }

    public String getCreditCardExpDate() {
        return "CREDIT_CARD_EXP_DATE";
    }

    public String getCreditCardExpMonth() {
        return null;
    }

    public String getCreditCardExpYear() {
        return null;
    }

    public String getCreditCardCvv() {
        return "CREDIT_CARD_CVV";
    }

    public String getBillToAddressFirstName() {
        return "NULL_BILLING_FIRST_NAME";
    }

    public String getBillToAddressLastName() {
        return "NULL_BILLING_LAST_NAME";
    }

    public String getBillToAddressCompanyName() {
        return "NULL_BILLING_COMPANY_NAME";
    }

    public String getBillToAddressLine1() {
        return "NULL_BILLING_ADDRESS_LINE1";
    }

    public String getBillToAddressLine2() {
        return "NULL_BILLING_ADDRESS_LINE2";
    }

    public String getBillToAddressCityLocality() {
        return "NULL_BILLING_CITY";
    }

    public String getBillToAddressStateRegion() {
        return "NULL_BILLING_STATE";
    }

    public String getBillToAddressPostalCode() {
        return "NULL_BILLING_ZIP";
    }

    public String getBillToAddressCountryCode() {
        return "NULL_BILLING_COUNTRY";
    }

    public String getBillToAddressPhone() {
        return "NULL_BILLING_PHONE";
    }

    public String getBillToAddressEmail() {
        return "NULL_BILLING_EMAIL";
    }

    public String getShipToAddressFirstName() {
        return null;
    }

    public String getShipToAddressLastName() {
        return null;
    }

    public String getShipToAddressCompanyName() {
        return null;
    }

    public String getShipToAddressLine1() {
        return null;
    }

    public String getShipToAddressLine2() {
        return null;
    }

    public String getShipToAddressCityLocality() {
        return null;
    }

    public String getShipToAddressStateRegion() {
        return null;
    }

    public String getShipToAddressPostalCode() {
        return null;
    }

    public String getShipToAddressCountryCode() {
        return null;
    }

    public String getShipToAddressPhone() {
        return null;
    }

    public String getShipToAddressEmail() {
        return null;
    }

    public PaymentGatewayType getHandlerType() {
        return this.paymentGatewayConfiguration.getGatewayType();
    }
}
