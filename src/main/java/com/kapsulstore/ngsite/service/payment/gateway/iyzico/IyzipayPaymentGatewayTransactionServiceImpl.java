package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.CreditCardValidator;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentAdditionalFieldType;
import org.broadleafcommerce.common.payment.PaymentGatewayRequestType;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.CreditCardDTO;
import org.broadleafcommerce.common.payment.dto.PaymentRequestDTO;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayTransactionService;
import org.broadleafcommerce.common.payment.service.FailureCountExposable;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.broadleafcommerce.common.vendor.service.type.ServiceStatusType;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayTransactionService")
public class IyzipayPaymentGatewayTransactionServiceImpl extends AbstractPaymentGatewayTransactionService implements FailureCountExposable {
    protected Integer failureCount = Integer.valueOf(0);
    protected Boolean isUp = Boolean.valueOf(true);

    public IyzipayPaymentGatewayTransactionServiceImpl() {
    }

    public PaymentResponseDTO authorize(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        return this.commonCreditCardProcessing(paymentRequestDTO, PaymentTransactionType.AUTHORIZE);
    }

    public PaymentResponseDTO capture(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        PaymentResponseDTO responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
        responseDTO.valid(true).paymentTransactionType(PaymentTransactionType.CAPTURE).amount(new Money(paymentRequestDTO.getTransactionTotal())).rawResponse("Successful Capture").successful(true);
        return responseDTO;
    }

    public PaymentResponseDTO authorizeAndCapture(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        return this.commonCreditCardProcessing(paymentRequestDTO, PaymentTransactionType.AUTHORIZE_AND_CAPTURE);
    }

    public PaymentResponseDTO reverseAuthorize(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        PaymentResponseDTO responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
        responseDTO.valid(true).paymentTransactionType(PaymentTransactionType.REVERSE_AUTH).amount(new Money(paymentRequestDTO.getTransactionTotal())).rawResponse("Successful Reverse Authorization").successful(true);
        return responseDTO;
    }

    public PaymentResponseDTO refund(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        PaymentResponseDTO responseDTO;
        if(paymentRequestDTO.getAdditionalFields().containsKey(PaymentGatewayRequestType.DETACHED_CREDIT_REFUND.getType())) {
            responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
            responseDTO.valid(true).paymentTransactionType(PaymentTransactionType.DETACHED_CREDIT).amount(new Money(paymentRequestDTO.getTransactionTotal())).rawResponse("Successful Detached Credit").successful(true);
            return responseDTO;
        } else {
            responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
            responseDTO.valid(true).paymentTransactionType(PaymentTransactionType.REFUND).amount(new Money(paymentRequestDTO.getTransactionTotal())).rawResponse("Successful Refund").successful(true);
            return responseDTO;
        }
    }

    public PaymentResponseDTO voidPayment(PaymentRequestDTO paymentRequestDTO) throws PaymentException {
        PaymentResponseDTO responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
        responseDTO.valid(true).paymentTransactionType(PaymentTransactionType.VOID).amount(new Money(paymentRequestDTO.getTransactionTotal())).rawResponse("Successful Reverse Authorization").successful(true);
        return responseDTO;
    }

    protected PaymentResponseDTO commonCreditCardProcessing(PaymentRequestDTO requestDTO, PaymentTransactionType paymentTransactionType) {
        this.setupNoncePaymentRequest(requestDTO);
        PaymentResponseDTO responseDTO = new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY);
        responseDTO.valid(true).paymentTransactionType(paymentTransactionType);
        CreditCardDTO creditCardDTO = requestDTO.getCreditCard();
        String transactionAmount = requestDTO.getTransactionTotal();
        String paymentToken = (String)requestDTO.getAdditionalFields().get(PaymentAdditionalFieldType.TOKEN.getType());
        CreditCardValidator visaValidator = new CreditCardValidator(2);
        CreditCardValidator amexValidator = new CreditCardValidator(1);
        CreditCardValidator mcValidator = new CreditCardValidator(4);
        CreditCardValidator discoverValidator = new CreditCardValidator(8);
        if(StringUtils.isNotBlank(transactionAmount) && StringUtils.isNotBlank(paymentToken)) {
            responseDTO.amount(new Money(requestDTO.getTransactionTotal())).rawResponse("Success!").successful(true);
        } else if(StringUtils.isNotBlank(transactionAmount) && creditCardDTO != null && StringUtils.isNotBlank(creditCardDTO.getCreditCardNum()) && (StringUtils.isNotBlank(creditCardDTO.getCreditCardExpDate()) || StringUtils.isNotBlank(creditCardDTO.getCreditCardExpMonth()) && StringUtils.isNotBlank(creditCardDTO.getCreditCardExpYear()))) {
            boolean validCard = false;
            if(visaValidator.isValid(creditCardDTO.getCreditCardNum())) {
                validCard = true;
            } else if(amexValidator.isValid(creditCardDTO.getCreditCardNum())) {
                validCard = true;
            } else if(mcValidator.isValid(creditCardDTO.getCreditCardNum())) {
                validCard = true;
            } else if(discoverValidator.isValid(creditCardDTO.getCreditCardNum())) {
                validCard = true;
            }

            boolean validDateFormat = false;
            boolean validDate = false;
            String[] parsedDate = null;
            if(StringUtils.isNotBlank(creditCardDTO.getCreditCardExpDate())) {
                parsedDate = creditCardDTO.getCreditCardExpDate().split("/");
            } else {
                parsedDate = new String[]{creditCardDTO.getCreditCardExpMonth(), creditCardDTO.getCreditCardExpYear()};
            }

            if(parsedDate.length == 2) {
                String expMonth = parsedDate[0];
                String expYear = parsedDate[1];

                try {
                    DateTime expirationDate = new DateTime(Integer.parseInt("20" + expYear), Integer.parseInt(expMonth), 1, 0, 0);
                    expirationDate = expirationDate.dayOfMonth().withMaximumValue();
                    validDate = expirationDate.isAfterNow();
                    validDateFormat = true;
                } catch (Exception var18) {
                    ;
                }
            }

            if(validDate && validDateFormat) {
                if(!validCard) {
                    responseDTO.amount(new Money(0)).rawResponse("cart.payment.card.invalid").successful(false);
                } else {
                    responseDTO.amount(new Money(requestDTO.getTransactionTotal())).rawResponse("Success!").successful(true);
                }
            } else {
                responseDTO.amount(new Money(0)).rawResponse("cart.payment.expiration.invalid").successful(false);
            }
        } else {
            responseDTO.amount(new Money(0)).rawResponse("cart.payment.invalid").successful(false);
        }

        return responseDTO;
    }

    protected void setupNoncePaymentRequest(PaymentRequestDTO requestDTO) {
        String nonce = (String)requestDTO.getAdditionalFields().get("PAYMENT_METHOD_NONCE");
        if(nonce != null) {
            String[] fields = nonce.split("\\|");
            requestDTO.creditCard().creditCardNum(fields[0]).creditCardHolderName(fields[1]).creditCardExpDate(fields[2]).creditCardCvv(fields[3]);
        }

    }

    public ServiceStatusType getServiceStatus() {
        return this.isUp.booleanValue()?ServiceStatusType.UP:ServiceStatusType.DOWN;
    }

    public synchronized void clearStatus() {
        this.isUp = Boolean.valueOf(true);
        this.failureCount = Integer.valueOf(0);
    }

    public synchronized void incrementFailure() {
        if(this.failureCount.intValue() >= this.getFailureReportingThreshold().intValue()) {
            this.isUp = Boolean.valueOf(false);
        } else {
            Integer var1 = this.failureCount;
            Integer var2 = this.failureCount = Integer.valueOf(this.failureCount.intValue() + 1);
        }

    }

    public Integer getFailureReportingThreshold() {
        return Integer.valueOf(3);
    }
}
