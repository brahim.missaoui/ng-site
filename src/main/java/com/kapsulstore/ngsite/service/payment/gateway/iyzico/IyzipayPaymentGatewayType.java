package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.PaymentGatewayType;

/**
 * Created by mehmet on 7/12/17.
 */

public class IyzipayPaymentGatewayType extends PaymentGatewayType {
    public static final PaymentGatewayType IYZICO_GATEWAY = new PaymentGatewayType("IYZICO_GATEWAY", "Iyzico Payment Gateway Implementation");
    public static final PaymentGatewayType IYZICO_HOSTED_GATEWAY = new PaymentGatewayType("IYZICO_HOSTED_GATEWAY", "Iyzico Hosted Payment Gateway Implementation");

    public IyzipayPaymentGatewayType() {
    }
}
