package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.apache.commons.lang.ArrayUtils;
import org.broadleafcommerce.common.money.Money;
import org.broadleafcommerce.common.payment.PaymentAdditionalFieldType;
import org.broadleafcommerce.common.payment.PaymentTransactionType;
import org.broadleafcommerce.common.payment.PaymentType;
import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.AbstractPaymentGatewayWebResponseService;
import org.broadleafcommerce.common.payment.service.PaymentGatewayWebResponsePrintService;
import org.broadleafcommerce.common.vendor.service.exception.PaymentException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayWebResponseService")
public class IyzipayPaymentGatewayWebResponseServiceImpl extends AbstractPaymentGatewayWebResponseService {
    @Resource(
        name = "blPaymentGatewayWebResponsePrintService"
    )
    protected PaymentGatewayWebResponsePrintService webResponsePrintService;
    @Resource(
        name = "iyzipayPaymentGatewayConfiguration"
    )
    protected IyzipayPaymentGatewayConfiguration configuration;

    public IyzipayPaymentGatewayWebResponseServiceImpl() {
    }

    public PaymentResponseDTO translateWebResponse(HttpServletRequest request) throws PaymentException {
        PaymentResponseDTO responseDTO = (new PaymentResponseDTO(PaymentType.CREDIT_CARD, IyzipayPaymentGatewayType.IYZICO_GATEWAY)).rawResponse(this.webResponsePrintService.printRequest(request));
        Map<String, String[]> paramMap = request.getParameterMap();
        Money amount = Money.ZERO;
        if(paramMap.containsKey("TRANSACTION_AMT")) {
            String amt = ((String[])paramMap.get("TRANSACTION_AMT"))[0];
            amount = new Money(amt);
        }

        boolean approved = false;
        if(paramMap.containsKey("SUCCESS")) {
            String[] msg = (String[])paramMap.get("SUCCESS");
            if(ArrayUtils.contains(msg, "true")) {
                approved = true;
            }
        }

        PaymentTransactionType type = PaymentTransactionType.AUTHORIZE_AND_CAPTURE;
        if(!this.configuration.isPerformAuthorizeAndCapture()) {
            type = PaymentTransactionType.AUTHORIZE;
        }

        responseDTO.successful(approved);
        responseDTO.amount(amount);
        responseDTO.paymentTransactionType(type);
        responseDTO.orderId(this.parse(paramMap, "ORDER_ID"));
        responseDTO.customer().customerId(this.parse(paramMap, "CUSTOMER_ID")).done();
        responseDTO.paymentToken(this.parse(paramMap, "PAYMENT_TOKEN_ID"));
        responseDTO.responseMap("GATEWAY_TRANSACTION_ID", this.parse(paramMap, "GATEWAY_TRANSACTION_ID"));
        responseDTO.responseMap("MESSAGE", this.parse(paramMap, "MESSAGE"));
        responseDTO.responseMap(PaymentAdditionalFieldType.TOKEN.getType(), this.parse(paramMap, "PAYMENT_TOKEN_ID"));
        responseDTO.responseMap(PaymentAdditionalFieldType.LAST_FOUR.getType(), this.parse(paramMap, "CREDIT_CARD_LAST_FOUR"));
        responseDTO.responseMap(PaymentAdditionalFieldType.CARD_TYPE.getType(), this.parse(paramMap, "CREDIT_CARD_TYPE"));
        responseDTO.responseMap(PaymentAdditionalFieldType.NAME_ON_CARD.getType(), this.parse(paramMap, "CREDIT_CARD_NAME"));
        responseDTO.responseMap(PaymentAdditionalFieldType.EXP_DATE.getType(), this.parse(paramMap, "CREDIT_CARD_EXP_DATE"));
        responseDTO.billTo().addressFirstName(this.parse(paramMap, "NULL_BILLING_FIRST_NAME"));
        responseDTO.billTo().addressLastName(this.parse(paramMap, "NULL_BILLING_LAST_NAME"));
        responseDTO.billTo().addressLine1(this.parse(paramMap, "NULL_BILLING_ADDRESS_LINE1"));
        responseDTO.billTo().addressLine2(this.parse(paramMap, "NULL_BILLING_ADDRESS_LINE2"));
        responseDTO.billTo().addressCityLocality(this.parse(paramMap, "NULL_BILLING_CITY"));
        responseDTO.billTo().addressStateRegion(this.parse(paramMap, "NULL_BILLING_STATE"));
        responseDTO.billTo().addressPostalCode(this.parse(paramMap, "NULL_BILLING_ZIP"));
        responseDTO.billTo().addressCountryCode(this.parse(paramMap, "NULL_BILLING_COUNTRY"));
        responseDTO.billTo().addressPhone(this.parse(paramMap, "NULL_BILLING_PHONE"));
        responseDTO.billTo().addressEmail(this.parse(paramMap, "NULL_BILLING_EMAIL"));
        responseDTO.billTo().addressCompanyName(this.parse(paramMap, "NULL_BILLING_COMPANY_NAME")).done();
        responseDTO.creditCard().creditCardHolderName(this.parse(paramMap, "CREDIT_CARD_NAME"));
        responseDTO.creditCard().creditCardLastFour(this.parse(paramMap, "CREDIT_CARD_LAST_FOUR"));
        responseDTO.creditCard().creditCardType(this.parse(paramMap, "CREDIT_CARD_TYPE"));
        responseDTO.creditCard().creditCardExpDate(this.parse(paramMap, "CREDIT_CARD_EXP_DATE")).done();
        return responseDTO;
    }

    protected String parse(Map<String, String[]> paramMap, String param) {
        return paramMap.containsKey(param)?((String[])paramMap.get(param))[0]:null;
    }
}
