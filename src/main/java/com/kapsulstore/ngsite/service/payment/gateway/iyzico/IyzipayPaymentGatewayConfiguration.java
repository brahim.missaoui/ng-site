package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.service.PaymentGatewayConfiguration;

/**
 * Created by mehmet on 7/11/17.
 */
public interface IyzipayPaymentGatewayConfiguration extends PaymentGatewayConfiguration {
    String getTransparentRedirectUrl();

    String getTransparentRedirectReturnUrl();

    String getCustomerPaymentTransparentRedirectUrl();

    String getCustomerPaymentTransparentRedirectReturnUrl();
}
