package com.kapsulstore.ngsite.service.payment.gateway.iyzico.processor;

import org.broadleafcommerce.common.payment.dto.PaymentResponseDTO;
import org.broadleafcommerce.common.payment.service.PaymentGatewayConfiguration;
import org.broadleafcommerce.common.payment.service.PaymentGatewayTransparentRedirectService;
import org.broadleafcommerce.common.web.payment.processor.AbstractTRCreditCardExtensionHandler;
import org.broadleafcommerce.common.web.payment.processor.TRCreditCardExtensionManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayTRExtensionHandler")
public class IyzicoPaymentGatewayTRExtensionHandler extends AbstractTRCreditCardExtensionHandler {
    public static final String FORM_ACTION_URL = "TRANSPARENT_REDIRECT_URL";
    public static final String FORM_HIDDEN_PARAMS = "FORM_HIDDEN_PARAMS";
    @Resource(
        name = "blTRCreditCardExtensionManager"
    )
    protected TRCreditCardExtensionManager extensionManager;
    @Resource(
        name = "iyzipayPaymentGatewayTransparentRedirectService"
    )
    protected PaymentGatewayTransparentRedirectService transparentRedirectService;
    @Resource(
        name = "iyzipayPaymentGatewayConfiguration"
    )
    protected PaymentGatewayConfiguration configuration;

    public IyzicoPaymentGatewayTRExtensionHandler() {
    }

    @PostConstruct
    public void init() {
        if(this.isEnabled()) {
            this.extensionManager.registerHandler(this);
        }

    }

    public String getFormActionURLKey() {
        return "TRANSPARENT_REDIRECT_URL";
    }

    public String getHiddenParamsKey() {
        return "FORM_HIDDEN_PARAMS";
    }

    public PaymentGatewayConfiguration getConfiguration() {
        return this.configuration;
    }

    public PaymentGatewayTransparentRedirectService getTransparentRedirectService() {
        return this.transparentRedirectService;
    }

    public void populateFormParameters(Map<String, Map<String, String>> formParameters, PaymentResponseDTO responseDTO) {
        String actionUrl = (String)responseDTO.getResponseMap().get("TRANSPARENT_REDIRECT_URL");
        Map<String, String> actionValue = new HashMap();
        actionValue.put(this.getFormActionURLKey(), actionUrl);
        formParameters.put(this.getFormActionURLKey(), actionValue);
        Map<String, String> hiddenFields = new HashMap();
        if(responseDTO.getResponseMap().get("TRANSPARENT_REDIRECT_RETURN_URL") != null) {
            hiddenFields.put("TRANSPARENT_REDIRECT_RETURN_URL", ((String)responseDTO.getResponseMap().get("TRANSPARENT_REDIRECT_RETURN_URL")).toString());
        }

        if(responseDTO.getResponseMap().get("TRANSACTION_AMT") != null) {
            hiddenFields.put("TRANSACTION_AMT", ((String)responseDTO.getResponseMap().get("TRANSACTION_AMT")).toString());
        }

        if(responseDTO.getResponseMap().get("ORDER_ID") != null) {
            hiddenFields.put("ORDER_ID", ((String)responseDTO.getResponseMap().get("ORDER_ID")).toString());
        }

        if(responseDTO.getResponseMap().get("CUSTOMER_ID") != null) {
            hiddenFields.put("CUSTOMER_ID", ((String)responseDTO.getResponseMap().get("CUSTOMER_ID")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_FIRST_NAME") != null) {
            hiddenFields.put("NULL_BILLING_FIRST_NAME", ((String)responseDTO.getResponseMap().get("NULL_BILLING_FIRST_NAME")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_LAST_NAME") != null) {
            hiddenFields.put("NULL_BILLING_LAST_NAME", ((String)responseDTO.getResponseMap().get("NULL_BILLING_LAST_NAME")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_ADDRESS_LINE1") != null) {
            hiddenFields.put("NULL_BILLING_ADDRESS_LINE1", ((String)responseDTO.getResponseMap().get("NULL_BILLING_ADDRESS_LINE1")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_ADDRESS_LINE2") != null) {
            hiddenFields.put("NULL_BILLING_ADDRESS_LINE2", ((String)responseDTO.getResponseMap().get("NULL_BILLING_ADDRESS_LINE2")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_CITY") != null) {
            hiddenFields.put("NULL_BILLING_CITY", ((String)responseDTO.getResponseMap().get("NULL_BILLING_CITY")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_STATE") != null) {
            hiddenFields.put("NULL_BILLING_STATE", ((String)responseDTO.getResponseMap().get("NULL_BILLING_STATE")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_ZIP") != null) {
            hiddenFields.put("NULL_BILLING_ZIP", ((String)responseDTO.getResponseMap().get("NULL_BILLING_ZIP")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_BILLING_COUNTRY") != null) {
            hiddenFields.put("NULL_BILLING_COUNTRY", ((String)responseDTO.getResponseMap().get("NULL_BILLING_COUNTRY")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_FIRST_NAME") != null) {
            hiddenFields.put("NULL_SHIPPING_FIRST_NAME", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_FIRST_NAME")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_LAST_NAME") != null) {
            hiddenFields.put("NULL_SHIPPING_LAST_NAME", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_LAST_NAME")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_ADDRESS_LINE1") != null) {
            hiddenFields.put("NULL_SHIPPING_ADDRESS_LINE1", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_ADDRESS_LINE1")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_ADDRESS_LINE2") != null) {
            hiddenFields.put("NULL_SHIPPING_ADDRESS_LINE2", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_ADDRESS_LINE2")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_CITY") != null) {
            hiddenFields.put("NULL_SHIPPING_CITY", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_CITY")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_STATE") != null) {
            hiddenFields.put("NULL_SHIPPING_STATE", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_STATE")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_ZIP") != null) {
            hiddenFields.put("NULL_SHIPPING_ZIP", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_ZIP")).toString());
        }

        if(responseDTO.getResponseMap().get("NULL_SHIPPING_COUNTRY") != null) {
            hiddenFields.put("NULL_SHIPPING_COUNTRY", ((String)responseDTO.getResponseMap().get("NULL_SHIPPING_COUNTRY")).toString());
        }

        formParameters.put(this.getHiddenParamsKey(), hiddenFields);
    }
}
