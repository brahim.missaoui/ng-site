package com.kapsulstore.ngsite.service.payment.gateway.iyzico;

import org.broadleafcommerce.common.payment.service.*;
import org.broadleafcommerce.common.web.payment.expression.PaymentGatewayFieldExtensionHandler;
import org.broadleafcommerce.common.web.payment.processor.CreditCardTypesExtensionHandler;
import org.broadleafcommerce.common.web.payment.processor.TRCreditCardExtensionHandler;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by mehmet on 7/12/17.
 */

@Service("iyzipayPaymentGatewayHostedConfigurationService")
public class IyzipayPaymentGatewayHostedConfigurationServiceImpl extends AbstractPaymentGatewayConfigurationService {
    @Resource(
        name = "iyzipayPaymentGatewayHostedConfiguration"
    )
    protected IyzipayPaymentGatewayHostedConfiguration configuration;
    @Resource(
        name = "iyzipayPaymentGatewayHostedRollbackService"
    )
    protected PaymentGatewayRollbackService rollbackService;
    @Resource(
        name = "iyzipayPaymentGatewayHostedService"
    )
    protected PaymentGatewayHostedService hostedService;
    @Resource(
        name = "iyzipayPaymentGatewayHostedTransactionConfirmationService"
    )
    protected PaymentGatewayTransactionConfirmationService transactionConfirmationService;
    @Resource(
        name = "iyzipayPaymentGatewayHostedWebResponseService"
    )
    protected PaymentGatewayWebResponseService webResponseService;

    public IyzipayPaymentGatewayHostedConfigurationServiceImpl() {
    }

    public PaymentGatewayConfiguration getConfiguration() {
        return this.configuration;
    }

    public PaymentGatewayTransactionService getTransactionService() {
        return null;
    }

    public PaymentGatewayTransactionConfirmationService getTransactionConfirmationService() {
        return this.transactionConfirmationService;
    }

    public PaymentGatewayReportingService getReportingService() {
        return null;
    }

    public PaymentGatewayCreditCardService getCreditCardService() {
        return null;
    }

    public PaymentGatewayCustomerService getCustomerService() {
        return null;
    }

    public PaymentGatewaySubscriptionService getSubscriptionService() {
        return null;
    }

    public PaymentGatewayFraudService getFraudService() {
        return null;
    }

    public PaymentGatewayHostedService getHostedService() {
        return this.hostedService;
    }

    public PaymentGatewayRollbackService getRollbackService() {
        return this.rollbackService;
    }

    public PaymentGatewayWebResponseService getWebResponseService() {
        return this.webResponseService;
    }

    public PaymentGatewayTransparentRedirectService getTransparentRedirectService() {
        return null;
    }

    public TRCreditCardExtensionHandler getCreditCardExtensionHandler() {
        return null;
    }

    public PaymentGatewayFieldExtensionHandler getFieldExtensionHandler() {
        return null;
    }

    public CreditCardTypesExtensionHandler getCreditCardTypesExtensionHandler() {
        return null;
    }
}
