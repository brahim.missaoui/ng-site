package com.kapsulstore.ngsite.catalog.dao;

import org.broadleafcommerce.common.persistence.Status;
import org.broadleafcommerce.core.catalog.dao.ProductOptionDaoImpl;
import org.broadleafcommerce.core.catalog.domain.Sku;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Created by mehmet on 6/22/17.
 */
public class KSProductOptionDaoImpl extends ProductOptionDaoImpl {
    @Override
    protected List<Long> filterCandidateSkusForArchivedStatus(List<Sku> candidateSkus) {
        List<Long> validCandidateSkuIds = new ArrayList();
        Iterator var3 = candidateSkus.iterator();

        while(var3.hasNext()) {
            Sku sku = (Sku)var3.next();
            validCandidateSkuIds.add(sku.getId());
        }

        return validCandidateSkuIds;
    }
}
