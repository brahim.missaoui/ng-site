import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { catalog } from './catalog.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CatalogService {
    private catalogBaseUrl = 'api/catalog';
    public currentCategory = new Subject<catalog.Category>();
    public currentCategory$ = this.currentCategory.asObservable();
    public currentProduct = new Subject<catalog.Product>();
    public currentProduct$ = this.currentProduct.asObservable();
    constructor(private http: Http) { }
    categoriesWrapper(): Observable<catalog.CategoriesWrapper> {
        return this.http.get(`${this.catalogBaseUrl}/categories`).map((res: Response) => res.json());
    }
    categoryList(): Observable<catalog.Category[]> {
        return this.http.get(`${this.catalogBaseUrl}/categoryList`).map((res: Response) => res.json());
    }
    category(catName: string): Observable<catalog.Category> {
        return this.http.get(`${this.catalogBaseUrl}/category/${catName}`).map((res: Response) => res.json());
    }
    public switchCategory(catName: string) {
        this.http.get(`${this.catalogBaseUrl}/category/${catName}`).map((res: Response) => res.json()).subscribe((data) =>
        this.currentCategory.next(data)
        );
    }
    public switchProduct(productId: number) {
        this.http.get(`${this.catalogBaseUrl}/product/${productId}`).map((res: Response) => res.json()).subscribe((data) =>
        this.currentProduct.next(data)
        );
    }
}
