export namespace catalog {

    export interface RetailPrice {
        amount: number;
        currency: string;
    }

    export class Media {
        public allowOverrideUrl: boolean;
        public id: number;
        public title: string;
        public url: string;
        public altText: string;
        public tags: string;
    }

    export interface AllowedValue {
        id: number;
        attributeValue: string;
        priceAdjustment?: any;
        productOptionId: number;
    }

    export interface ProductOption {
        attributeName: string;
        label: string;
        required: boolean;
        productOptionType: string;
        productOptionValidationStrategyType: string;
        productOptionValidationType: string;
        allowedValues: AllowedValue[];
        validationString: string;
    }

    export interface DefaultSku {
        id: number;
        activeStartDate: Date;
        activeEndDate?: any;
        name: string;
        active: boolean;
        available: boolean;
        inventoryType?: any;
        description?: any;
        retailPrice: RetailPrice;
        salePrice?: any;
        weight?: any;
        dimension?: any;
    }

    export interface AdditionalSku {
        id: number;
        activeStartDate: Date;
        activeEndDate?: any;
        name: string;
        active: boolean;
        available: boolean;
        inventoryType?: any;
        description?: any;
        retailPrice: RetailPrice;
        salePrice?: any;
        weight?: any;
        dimension?: any;
    }

    export interface Product {
        id: number;
        name: string;
        description?: any;
        longDescription: string;
        url: string;
        retailPrice: RetailPrice;
        salePrice?: any;
        primaryMedia: Media;
        dynamicSkuPrices?: any;
        active: boolean;
        productOptions: ProductOption[];
        priority?: any;
        bundleItemsRetailPrice?: any;
        bundleItemsSalePrice?: any;
        activeStartDate?: any;
        activeEndDate?: any;
        manufacturer?: any;
        model?: any;
        promoMessage?: any;
        defaultSku: DefaultSku;
        additionalSkus: AdditionalSku[];
        defaultCategoryId?: any;
        upsaleProducts?: any;
        crossSaleProducts?: any;
        productAttributes?: any;
        media?: any;
        skuBundleItems?: any;
        promotionMessages?: any;
    }

    export interface Category {
        id: number;
        name: string;
        description: string;
        active: boolean;
        url: string;
        urlKey: string;
        activeStartDate: Date;
        activeEndDate?: any;
        subcategories?: any;
        products: Product[];
        categoryAttributes?: any;
    }

    export class CategoriesWrapper {
        public categories?: any[];
        constructor(
            categories?: any[]
        ) {
            this.categories = categories ? categories : null;
        }
    }

}
