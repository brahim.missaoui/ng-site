export class Categoryx {
    public id?: number;
    public name?: string;
    public description?: string;
    public active?: boolean;
    public url?: string;
    public urlKey?: string;
    public activeStartDate?: Date;
    public activeEndDate?: Date;
    public subcategories?: any[];
    public products?: any[];
    public categoryAttributes?: any[];

    constructor(
        id?: number,
        name?: string,
        description? : string,
        active? : boolean,
        url? : string,
        urlKey? : string,
        activeStartDate? : Date,
        activeEndDate? : Date,
        subcategories? : any[],
        products? : any[],
        categoryAttributes? : any[]
    ) {
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.description = description ? description : null;
        this.active = active ? active : null;
        this.url = url ? url : null;
        this.urlKey = urlKey ? urlKey : null;
        this.activeStartDate = activeStartDate ? activeStartDate : null;
        this.activeEndDate = activeEndDate ? activeEndDate : null;
        this.subcategories = subcategories ? subcategories : null;
        this.products = products ? products : null;
        this.categoryAttributes = categoryAttributes ? categoryAttributes : null;
    }
}
