export namespace cart {
    export interface Money {
        amount: number;
        currency: string;
    }

    export class CustomerAttribute{
        id: number;
        name: string;
        value: string;
        customerId: number;
    }

    export class Customer {
        id: number;
        firstName: string;
        lastName: string;
        emailAddress: string;
        customerAttributes: CustomerAttribute[];
    }

    export interface Media {
        allowOverrideUrl: boolean;
        id: number;
        title: string;
        url: string;
        altText: string;
        tags: string;
    }

    export class OrderItemAttribute {
        id: number;
        name: string;
        value: string;
        orderItemId: number;
        constructor(name: string, value: string){
            this.name = name;
            this.value = value;
        }
    }

    export interface OrderItemPriceDetail {
        id: number;
        totalAdjustmentValue: Money;
        totalAdjustedPrice: Money;
        quantity: number;
        orderItemPriceDetailAdjustments: any[];
    }


    export class ISOCountry {
        name: string;
        alpha2: string;
    }
    export class Country {
        name: string;
        abbreviation: string;
    }
    export class State {
        name: string;
        abbreviation: string;
    }

    export class Phone {
        id: number;
        phoneNumber: string;
        isActive: boolean;
        isDefault: boolean;
    }

    export class Address {
        id: number;
        firstName: string;
        lastName: string;
        addressLine1: string;
        addressLine2: string;
        addressLine3: string;
        city: string;
        state: State;
        country: Country;
        isoCountrySubdivision: string;
        stateProvinceRegion: string;
        isoCountryAlpha2: string;
        postalCode: string;
        phonePrimary: Phone;
        phoneSecondary: Phone;
        phoneFax: Phone;
        companyName: string;
        isBusiness: boolean;
        isDefault: boolean;
    }

    export class OrderItem {
        id: number;
        name: string;
        quantity: number;
        retailPrice: Money;
        salePrice: Money;
        orderId: number;
        categoryId: number;
        skuId: number;
        productId: number;
        productUrl: string;
        primaryMedia: Media;
        isBundle: boolean;
        parentOrderItemId?: any;
        orderItemAttributes: OrderItemAttribute[];
        orderItemPriceDetails: OrderItemPriceDetail[];
        bundleItems?: any;
        childOrderItems: any[];
        qualifiers?: any;
        isDiscountingAllowed: boolean;
    }

    export interface Media {
        allowOverrideUrl: boolean;
        id: number;
        title: string;
        url: string;
        altText: string;
        tags: string;
    }

    export interface FulfillmentGroupItem {
        id: number;
        fulfillmentGroupId: number;
        orderItemId: number;
        orderItem: OrderItem;
        totalTax: Money;
        quantity: number;
        totalItemAmount: Money;
        taxDetails?: any;
    }

    export interface FulfillmentGroup {
        id: number;
        orderId: number;
        fulfillmentType?: any;
        fulfillmentOption?: any;
        total: Money;
        address?: any;
        phone?: any;
        fulfillmentGroupAdjustments?: any;
        fulfillmentGroupItems: FulfillmentGroupItem[];
        taxDetails?: any;
    }

    export interface Cart {
        id: number;
        orderNumber?: any;
        status: string;
        totalTax: Money;
        totalShipping: Money;
        subTotal: Money;
        total: Money;
        itemCount: number;
        customer: Customer;
        orderItems: OrderItem[];
        fulfillmentGroups: FulfillmentGroup[];
        payments?: any;
        orderAdjustments?: any;
        orderAttributes?: any;
        cartMessages?: any;
    }

}


