import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { cart } from './cart.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CartService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private cartBaseUrl = 'api/cart';
    public currentCartId: number;
    public currentCart = new ReplaySubject<cart.Cart>();
    public currentCart$ = this.currentCart.asObservable();
    public pendingAddToCart:boolean = false;
    public paymentMethod:string;

    constructor(private http: Http) { 
      this.loadCart();
    }

    public removeOrderItem(cart: cart.Cart, orderItem: cart.OrderItem): Promise<cart.Cart>  {
      return this.http.delete(`${this.cartBaseUrl}/${cart.id}/items/${orderItem.id}`).map((res: Response) => res.json()).toPromise()
    }

    public updateOrderItemQuantity(cartId: number, orderItemId: number, quantity: number): Promise<cart.Cart>  {
      return this.http.put(`${this.cartBaseUrl}/${cartId}/items/${orderItemId}`, {}, { params: { quantity: quantity} } ).map((res: Response) => res.json()).toPromise();
    }

    public promiseCart(): Promise<cart.Cart>  {
      return this.http.get(`${this.cartBaseUrl}`).map((res: Response) => res.json()).toPromise()
    }

    public loadCart()  {
      this.http.get(`${this.cartBaseUrl}`).map((res: Response) => res.json()).subscribe((data) =>  this.currentCart.next(data));
    }
    
  public addItemToCartOrder(cartId: number, item: cart.OrderItem, successCallBack: Function, catchCallBack: Function): Promise<void>{
      let url = `${this.cartBaseUrl}/${cartId}/item`;
      return this.http.post(url, JSON.stringify(item), {headers: this.headers}).map(res => res.json()).toPromise().then((result) => this.updateCartCount(successCallBack)).catch((err)=>this.logAddItemToCartOrderError(err, catchCallBack));
  }

  private logAddItemToCartOrderError(err, catchCallBack: Function){
    console.log(err);
    this.pendingAddToCart = false;
    catchCallBack(err)
  }

  private updateCartCount(successCallBack : Function){
    this.loadCart();
    this.pendingAddToCart = false;
    successCallBack();
  }

  public addItemToOrder(item: cart.OrderItem, successCallBack: Function, catchCallBack: Function ){
      this.pendingAddToCart = true;
      let pCart: Promise<cart.Cart> = this.promiseCart();
      pCart.then((cart) => this.addItemToCartOrder(cart.id, item,successCallBack, catchCallBack)).catch((err)=>catchCallBack(err));
   }

}
