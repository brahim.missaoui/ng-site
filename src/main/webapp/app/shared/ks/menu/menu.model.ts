export class Menu {
    public label?: string;
    public url?: string;
    public imageUrl?: string;
    public altText?: string;
    public categoryId?: number;
    public submenu?: any;

    constructor(
        label?: string,
        url?: string,
        imageUrl?: string,
        altText?: string,
        categoryId?: number,
        submenu?: any
    ) {
        this.label = label ? label : null;
        this.url = url ? url : null;
        this.imageUrl = imageUrl ? imageUrl : null;
        this.altText = altText ? altText : null;
        this.categoryId = categoryId ? categoryId : null;
        this.submenu = submenu ? submenu : null;
    }
}
