import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Menu } from './menu.model';

@Injectable()
export class MenuService {
    private resourceUrl = 'api/menu';

    constructor(private http: Http) { }

    list(): Observable<Menu[]> {
        return this.http.get(`${this.resourceUrl}/all`).map((res: Response) => res.json());
    }

    listMenu(navName: string): Observable<Menu[]> {
        return this.http.get(`${this.resourceUrl}/${navName}`).map((res: Response) => res.json());
    }

}
