export namespace checkout {
    export class OrderPaymentWrapper {
            active: boolean;
            amount: number;
            billingAddress: AddressWrapper;
            currency: string;
            gatewayType: string;
            id: number;
            orderId: number;
            referenceNumber: string;
            transactions: PaymentTransactionWrapper[];
            type: string;
    }

    export class AddressWrapper {
            addressLine1: string;
            addressLine2: string;
            addressLine3: string;
            city: string;
            companyName: string;
            country: CountryWrapper;
            firstName: string;
            id: number;
            isBusiness: boolean;
            isDefault: boolean;
            isoCountryAlpha2: ISOCountryWrapper;
            isoCountrySubdivision: string;
            lastName: string;
            phoneFax: PhoneWrapper;
            phonePrimary: PhoneWrapper;
            phoneSecondary: PhoneWrapper;
            postalCode: string;
            state: StateWrapper;
            stateProvinceRegion: string;
    }

    export class PaymentTransactionWrapper {
        additionalFields: MapElementWrapper[];
        amount: number;
        archived: string;
        currency: string;
        customerIpAddress: string;
        id: number;
        orderPaymentId: number;
        parentTransactionId: number;
        rawResponse: string;
        success: boolean;
        type: string;
    }

    export class CountryWrapper {
        abbreviation: string;
        name: string;
    }

    export class ISOCountryWrapper {
        alpha2: string;
        name: string;
    }

    export class PhoneWrapper {
        id: number;
        isActive: boolean;
        isDefault: boolean;
        phoneNumber: string;
    }

    export class StateWrapper {
        abbreviation: string;
        name: string;
    }

    export class MapElementWrapper {
        key: string;
        value: string;
    }

}

export namespace anonymousCheckout {

    export class CheckoutAddress {
        public address1: string;
        public address2: string;
        public addressName: string;
        public city: string;
        public name: string;
        public phone: string;
        public postalCode: string;
        public surname: string;
    } 

    export class AnonymousCheckoutModel {
        public customerEMail: string;
        public shippingAddress: CheckoutAddress;
        public billingAddress: CheckoutAddress;
    }

}

export namespace iyzico {
    export class CheckoutFormInitialize {
        checkoutFormContent: string;
        conversationId: string;
        errorCode: string;
        errorGroup: string;
        errorMessage: string;
        locale: string;
        paymentPageUrl: string;
        status: string;
        systemTime: number;
        token: string;
        tokenExpireTime: number;
    }
}

