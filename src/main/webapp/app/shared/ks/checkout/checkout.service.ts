import { Injectable } from '@angular/core';
import { checkout, anonymousCheckout } from './checkout.model';
import { Http, Response, URLSearchParams, RequestOptionsArgs, Headers  } from "@angular/http";


@Injectable()
export class CheckoutService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private baseUrl = 'api/checkout';
    public currentCartId: number;

    public lastOrderPaymet: checkout.OrderPaymentWrapper;
    
    public pendingAddToCart:boolean = false;
    public paymentMethod:string;

    constructor(private http: Http) { }


    public iyzipayPaymentRedirect(checkoutModel: anonymousCheckout.AnonymousCheckoutModel , cartId: number, successCallBack: Function, catchCallBack: Function): Promise<void>{
      let url = `api/iyzipay/transaction/anonymous/${cartId}`;
      //return this.http.get(`api/iyzipay/tansaction/anonymous/${cartId}`).map((res: Response) => res.json()).toPromise().then((result) => successCallBack(result)).catch((err) => catchCallBack(err));
      return this.http.post(url, JSON.stringify(checkoutModel), {headers: this.headers}).map(res => res.json()).toPromise().then((result) => successCallBack(result)).catch((err)=>catchCallBack(err));
    }
    public codCheckoutComplete(checkoutModel: anonymousCheckout.AnonymousCheckoutModel , cartId: number, successCallBack: Function, catchCallBack: Function): Promise<void>{
      let url = `/api/checkout/cod/complete/${cartId}`;
      //return this.http.get(`api/iyzipay/tansaction/anonymous/${cartId}`).map((res: Response) => res.json()).toPromise().then((result) => successCallBack(result)).catch((err) => catchCallBack(err));
      return this.http.post(url, JSON.stringify(checkoutModel), {headers: this.headers}).map(res => res.json()).toPromise().then((result) => successCallBack(result)).catch((err)=>catchCallBack(err));
    }

}
