import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpBreadcrumbComponent } from './smp-breadcrumb.component';

describe('SmpBreadcrumbComponent', () => {
  let component: SmpBreadcrumbComponent;
  let fixture: ComponentFixture<SmpBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
