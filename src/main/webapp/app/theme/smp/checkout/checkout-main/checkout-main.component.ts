import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { cart } from "../../../../shared/ks/cart/cart.model";
import { CartService } from "../../../../shared/ks/cart/cart.service";
import { CheckoutService } from "../../../../shared/ks/checkout/checkout.service";
import { checkout, anonymousCheckout, iyzico } from "../../../../shared/ks/checkout/checkout.model";
import { CheckoutPageNavigator, CheckoutAddress } from "./checkout-main.model";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'jhi-checkout-main',
  templateUrl: './checkout-main.component.html',
  styleUrls: ['./checkout-main.component.css']
})

//https://toddmotto.com/angular-2-forms-reactive
//https://auth0.com/blog/angular2-series-forms-and-custom-validation/

export class CheckoutMainComponent implements OnInit {
  public iyzicoCheckoutFormInitialize: iyzico.CheckoutFormInitialize;
  public iyzipayPaymentRedirectUrl: string;
  private orderPayment: checkout.OrderPaymentWrapper;
  public cart: cart.Cart;
  public nav: CheckoutPageNavigator = new CheckoutPageNavigator();
  
  public shippingAddressForm: FormGroup;
  public customerEMail: string;
  public shippingAddress: CheckoutAddress = new CheckoutAddress();
  public billingAddress: CheckoutAddress = new CheckoutAddress();
  public isBillingAddressShippingAddress: boolean = true;
  public isCheckoutCompletePending: boolean = false;

 public stepFromName: string = 'shippingAddressForm';
 public billingAddressForm: FormGroup;
 public paymentForm: FormGroup;
 public paymentMethod: string ='KREDI_KARTI';
 public saleDate: number = Date.now();

  constructor(private fb: FormBuilder, private cartService: CartService, private checkoutService: CheckoutService, private sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.cartService.promiseCart().then((cart)=>this.loadCartSuccessCallBack(cart)).catch((err)=>this.loadCartFailCallBack(err));
    this.shippingAddressForm = this.fb.group({
      email: [this.customerEMail, [Validators.required, Validators.email, Validators.minLength(2)]],
      name: [this.shippingAddress.name, ],
      surname: [this.shippingAddress.surname, [Validators.required, Validators.minLength(2)]],
      address1: [this.shippingAddress.address1, [Validators.required, Validators.minLength(2)]],
      address2: [this.shippingAddress.address2, ],
      city: [this.shippingAddress.city, [Validators.required, Validators.minLength(2)]],
      postalCode: [this.shippingAddress.postalCode, [Validators.required, Validators.minLength(2)]],
      phone: [this.shippingAddress.phone, [Validators.required, Validators.minLength(2)]],
      addressName: [this.shippingAddress.addressName, ],
    });

    this.billingAddressForm = this.fb.group({
      useShippingData: ['', ],
      name: ['', [Validators.required, Validators.minLength(2)]],
      surname: ['', [Validators.required, Validators.minLength(2)]],
      address1: ['', [Validators.required, Validators.minLength(2)]],
      address2: ['', ],
      city: ['', [Validators.required, Validators.minLength(2)]],
      postalCode: ['', [Validators.required, Validators.minLength(2)]],
      phone: ['', [Validators.minLength(10)]],
      addressName: ['', ],
    });

    this.paymentForm = this.fb.group({
      paymentMethod: ['', [Validators.required]],
    });
    //identityNo: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern('[0-9]*')]],
  }
  
  private getAnonymousCheckoutModel():anonymousCheckout.AnonymousCheckoutModel{
    this.cart.customer.emailAddress = this.customerEMail;
    this.cart.customer.firstName = this.billingAddress.name;
    this.cart.customer.lastName = this.billingAddress.surname;
    


    let payment: checkout.OrderPaymentWrapper = new checkout.OrderPaymentWrapper();
    payment.billingAddress = new checkout.AddressWrapper();
    //payment.billingAddress.companyName = this.billingAddress.
    payment.billingAddress.country = new checkout.CountryWrapper();
    payment.billingAddress.country.abbreviation = 'TR';
    payment.billingAddress.country.name = 'Turkey';
    payment.billingAddress.firstName = this.billingAddress.name;
    payment.billingAddress.lastName = this.billingAddress.surname;
    payment.billingAddress.isBusiness = false;
    payment.billingAddress.isDefault = true;
    payment.billingAddress.isoCountryAlpha2 = new checkout.ISOCountryWrapper();
    payment.billingAddress.isoCountryAlpha2.alpha2 = 'TR';
    payment.billingAddress.isoCountryAlpha2.name = 'Turkey';
    //payment.billingAddress.isoCountrySubdivision
    //payment.billingAddress.phoneFax
    payment.billingAddress.phonePrimary = new checkout.PhoneWrapper();
    //payment.billingAddress.phonePrimary.id
    payment.billingAddress.phonePrimary.isActive = true;
    payment.billingAddress.phonePrimary.isDefault = true;
    payment.billingAddress.phonePrimary.phoneNumber = this.billingAddress.phone;
    payment.billingAddress.postalCode = this.billingAddress.postalCode;
    //payment.billingAddress.state = new checkout.StateWrapper();
    
    payment.billingAddress.city = this.billingAddress.city;
    payment.billingAddress.addressLine1 = this.billingAddress.address1;
    payment.billingAddress.addressLine2 = this.billingAddress.address2;
    payment.billingAddress.addressLine3 = this.billingAddress.postalCode;

    payment.type='CREDIT_CARD';
    payment.gatewayType='Passthrough';
    payment.amount = this.cart.total.amount;
    payment.currency = this.cart.total.currency;
    
    
    //this.checkoutService.addPaymentToOrder(payment, this.cart.id, this.addPaymentToOrderSuccessCallBack, this.addPaymentToOrderFailCallBack);
    let model: anonymousCheckout.AnonymousCheckoutModel = new anonymousCheckout.AnonymousCheckoutModel();
    model.billingAddress = this.billingAddress;
    model.customerEMail = this.customerEMail;
    model.shippingAddress = this.shippingAddress;
    return model;
  }

  onShippingFormSubmit(form: any){
    if(this.shippingAddressForm.touched || this.shippingAddressForm.dirty){
      if(this.shippingAddressForm.valid){
        this.nav.isShippingAddressCompleted = true;
        console.log('switch to billingAddressForm');
      }else{
        console.log('shippingAddressForm is not valid');
      }
    }else{
      console.log('shippingAddressForm is not touched');
    }
  }

  private setBillingDataFromShippingData() :void{
    this.billingAddress.name = this.shippingAddress.name;
    this.billingAddress.surname = this.shippingAddress.surname;
    this.billingAddress.address1 = this.shippingAddress.address1;
    this.billingAddress.address2 = this.shippingAddress.address2;
    this.billingAddress.city = this.shippingAddress.city;
    this.billingAddress.postalCode = this.shippingAddress.postalCode;
    this.billingAddress.phone = this.shippingAddress.phone
    this.billingAddress.addressName = this.shippingAddress.addressName;
  }

  onBillingFormSubmit(form: any){
    if(this.billingAddressForm.touched || this.billingAddressForm.dirty){
      if(this.billingAddressForm.get('useShippingData').value){
        this.setBillingDataFromShippingData();
        this.nav.isBillingAddressCompleted = true;
        console.log('switch to paymentForm');
        return;
      }else{
        if(this.shippingAddressForm.valid){
          this.nav.isBillingAddressCompleted = true;
          console.log('switch to paymentForm');
        }else{
          console.log('billingAddressForm is not valid');
        }
      }
    }else{
      console.log('billingAddressForm is not touched');
    }
  }

  private loadCartSuccessCallBack(cart: cart.Cart){
    this.cart = cart;
    this.cartService.loadCart();
  }

  private loadCartFailCallBack(err){
    console.log('loadCartFailCallBack' + err);
  }


  public completeOrder(): void{
    this.isCheckoutCompletePending = true;
    let model: anonymousCheckout.AnonymousCheckoutModel = this.getAnonymousCheckoutModel();
    
    if(this.paymentMethod=='KREDI_KARTI'){
      this.checkoutService.iyzipayPaymentRedirect(model, this.cart.id, this.iyzipayPaymentRedirectSuccessCallBack.bind(this), this.iyzipayPaymentRedirectFailCallBack.bind(this))
    }
  }

  public onIyzicoPaymentFormSubmit(): void{  
    let model: anonymousCheckout.AnonymousCheckoutModel = this.getAnonymousCheckoutModel();
    this.checkoutService.iyzipayPaymentRedirect(model, this.cart.id, this.iyzipayPaymentRedirectSuccessCallBack.bind(this), this.iyzipayPaymentRedirectFailCallBack)
  }

  private iyzipayPaymentRedirectSuccessCallBack(iyzicoForm: iyzico.CheckoutFormInitialize){
    this.isCheckoutCompletePending = false;
    this.nav.isOrderApproveFormCompleted = true;
    this.iyzicoCheckoutFormInitialize = iyzicoForm;
  }

  private iyzipayPaymentRedirectFailCallBack(err){
    this.isCheckoutCompletePending = false;
    console.log('iyzipayPaymentRedirectFailCallBack' + err);
  }



}

