
export class CheckoutAddress {
  public name: string;
  public surname: string;
  public address1: string;
  public address2: string;
  public city: string;
  public postalCode: string;
  public phone: string;
  public addressName: string;
}

export class CheckoutPageNavigator {
  public isShippingAddressCompleted: boolean = false;
  public isBillingAddressCompleted: boolean = false;
  public isDistanceSaleAgreementCompleted: boolean = false;
  public isPreInfoFormCompleted: boolean = false;
  public isOrderApproveFormCompleted: boolean = false;
  public reset(): void{
    this.isBillingAddressCompleted = false;
    this.isDistanceSaleAgreementCompleted = false;
    this.isPreInfoFormCompleted = false;
    this.isShippingAddressCompleted = false;
    this.isOrderApproveFormCompleted = false;
  }
  public canShowShipping(): boolean{
    return !this.isShippingAddressCompleted;
  }
  public canShowBilling(): boolean{
    return this.isShippingAddressCompleted && !this.isBillingAddressCompleted;
  }

  public canShowPreInfo(): boolean{
    return this.isShippingAddressCompleted && this.isBillingAddressCompleted &&
    !this.isPreInfoFormCompleted;
  }

  public canShowDistanceSale(): boolean{
    return this.isShippingAddressCompleted && this.isBillingAddressCompleted &&
    this.isPreInfoFormCompleted &&
    !this.isDistanceSaleAgreementCompleted;
  }
  public canShowOrderApprove(): boolean{
    return this.isShippingAddressCompleted && this.isBillingAddressCompleted &&
    this.isPreInfoFormCompleted &&
    this.isDistanceSaleAgreementCompleted &&
    !this.isOrderApproveFormCompleted;
  }
  

  public isAllCompleted(): boolean{
    return this.isBillingAddressCompleted &&
    this.isDistanceSaleAgreementCompleted &&
    this.isPreInfoFormCompleted &&
    this.isShippingAddressCompleted &&
    this.isOrderApproveFormCompleted
  }

}
