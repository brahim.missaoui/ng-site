import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../../shared/ks/cart/cart.service";
import { cart } from "../../../../shared/ks/cart/cart.model";

@Component({
  selector: 'jhi-checkout-sidebar',
  templateUrl: './checkout-sidebar.component.html',
  styleUrls: ['./checkout-sidebar.component.css']
})
export class CheckoutSidebarComponent implements OnInit {
  public cart: cart.Cart;

  constructor(private cartService: CartService) { }
  ngOnInit() {
    this.cartService.promiseCart().then((cart)=>this.loadCartSuccessCallBack(cart)).catch((err)=>this.loadCartFailCallBack(err));
  }

  private loadCartSuccessCallBack(cart: cart.Cart){
    this.cart = cart;
    this.cartService.loadCart();
  }

  private loadCartFailCallBack(err){
    console.log('loadCartFailCallBack' + err);
  }
}
