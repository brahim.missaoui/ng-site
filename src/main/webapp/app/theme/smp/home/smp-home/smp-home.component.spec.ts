import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpHomeComponent } from './smp-home.component';

describe('SmpHomeComponent', () => {
  let component: SmpHomeComponent;
  let fixture: ComponentFixture<SmpHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
