import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpCustomerLoginComponent } from './smp-customer-login.component';

describe('SmpCustomerLoginComponent', () => {
  let component: SmpCustomerLoginComponent;
  let fixture: ComponentFixture<SmpCustomerLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpCustomerLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpCustomerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
