import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpCustomerRegisterComponent } from './smp-customer-register.component';

describe('SmpCustomerRegisterComponent', () => {
  let component: SmpCustomerRegisterComponent;
  let fixture: ComponentFixture<SmpCustomerRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpCustomerRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpCustomerRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
