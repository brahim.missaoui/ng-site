import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpNewsComponent } from './smp-news.component';

describe('SmpNewsComponent', () => {
  let component: SmpNewsComponent;
  let fixture: ComponentFixture<SmpNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
