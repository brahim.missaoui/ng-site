import { Component, OnInit, OnChanges } from '@angular/core';
import { catalog } from '../../../../shared/ks/catalog/catalog.model';
import { CatalogService } from '../../../../shared/ks/catalog/catalog.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-smp-category-products',
  templateUrl: './smp-category-products.component.html',
  styleUrls: ['./smp-category-products.component.css']
})
export class SmpCategoryProductsComponent implements OnInit {
  public category: catalog.Category;
  sub: any;

  constructor(private catalogService: CatalogService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.catalogService.currentCategory$.subscribe((data) => this.category = data );
    this.catalogService.switchCategory(this.route.snapshot.params.catName);
    this.sub = this.route.params.subscribe(params => {
      this.catalogService.switchCategory(params.catName);
    });
  }

  public reloadByRoute(productId: number) {
    this.catalogService.switchProduct(productId);
  }
}
