import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { catalog } from '../../../../shared/ks/catalog/catalog.model';
import { CatalogService } from '../../../../shared/ks/catalog/catalog.service';
import { ActivatedRoute } from '@angular/router';
import { CartService } from "../../../../shared/ks/cart/cart.service";
import { cart } from "../../../../shared/ks/cart/cart.model";
import {ImageZoomModule} from 'angular2-image-zoom';
import { Subject } from "rxjs";


@Component({
  selector: 'jhi-smp-product',
  templateUrl: './smp-product.component.html',
  styles: []
})
export class SmpProductComponent implements OnInit, OnChanges {
  public pendingAddToCart: boolean = false;
  public addedToCart: boolean = false;

  public product: catalog.Product;
  public selectedMedia: catalog.Media;


  public addToCartForm: FormGroup = this.fb.group({
    quantity: [1, Validators.required]
  });

  constructor(public fb: FormBuilder,private catalogService: CatalogService,private cartService: CartService, private route: ActivatedRoute) {

   }

  ngOnInit() {
    this.catalogService.currentProduct$.subscribe((data) => this.buildAddToCartForm(data) );
  }

  private buildAddToCartForm(data: catalog.Product){
    this.product = data;
    if(this.product.productOptions){
      this.product.productOptions.forEach(element => {
        let control = new FormControl(element.attributeName, [Validators.required]);
        control.setValidators(Validators.required);
        this.addToCartForm.addControl(element.attributeName, control);
      });
    }    
    this.selectedMedia = data.primaryMedia;
  }

  public setSelectedMedia(media: catalog.Media){
    this.selectedMedia = media;
  }

  public addToCart(item: cart.OrderItem){
    this.pendingAddToCart = true;
    this.cartService.addItemToOrder(item, this.addToCartSuccessCallBack.bind(this), this.addToCartFailCallBack.bind(this));
  }
  public addToCartSuccessCallBack(){
    console.log('addToCartSuccessCallBack');
    this.pendingAddToCart = false;
    this.addedToCart = true;

  }

  public addToCartFailCallBack(err){
    console.log('addToCartFailCallBack');
    this.pendingAddToCart = false;
  }


  ngOnChanges(changes: SimpleChanges): void {
    throw new Error("Method not implemented.");
  }

  onSubmit(): void {
      let item = new cart.OrderItem();
      item.productId = this.product.id
      if(this.product.productOptions){
          try {
          this.product.productOptions.forEach(element => {
              let productOptionControl = this.addToCartForm.controls[element.attributeName];
              let orderItemAttribute = new cart.OrderItemAttribute(element.attributeName,  productOptionControl.value);
              if(!item.orderItemAttributes){
                item.orderItemAttributes = [orderItemAttribute];
              }else{
                item.orderItemAttributes.push(orderItemAttribute);
              }

          });
        } catch (error) {
          console.log(error);
        }
      }
      item.quantity = 1;
    console.log(this.addToCartForm.value);  // {first: 'Nancy', last: 'Drew'}
    this.addToCart(item);
  }
}
