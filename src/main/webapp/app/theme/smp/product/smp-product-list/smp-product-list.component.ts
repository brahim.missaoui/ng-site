import { Component, OnInit, Input } from '@angular/core';
import { catalog } from '../../../../shared/ks/catalog/catalog.model';

@Component({
  selector: 'jhi-smp-product-list',
  templateUrl: './smp-product-list.component.html',
  styles: []
})
export class SmpProductListComponent implements OnInit {
  @Input()
  public products: catalog.Product[];

  constructor() { }

  ngOnInit() {
  }

}
