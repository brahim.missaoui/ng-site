import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpContactUsComponent } from './smp-contact-us.component';

describe('SmpContactUsComponent', () => {
  let component: SmpContactUsComponent;
  let fixture: ComponentFixture<SmpContactUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpContactUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
