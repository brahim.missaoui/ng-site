import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpMainContentComponent } from './smp-main-content.component';

describe('SmpMainContentComponent', () => {
  let component: SmpMainContentComponent;
  let fixture: ComponentFixture<SmpMainContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpMainContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpMainContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
