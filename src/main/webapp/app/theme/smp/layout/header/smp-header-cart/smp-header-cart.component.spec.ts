import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpHeaderCartComponent } from './smp-header-cart.component';

describe('SmpHeaderCartComponent', () => {
  let component: SmpHeaderCartComponent;
  let fixture: ComponentFixture<SmpHeaderCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpHeaderCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpHeaderCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
