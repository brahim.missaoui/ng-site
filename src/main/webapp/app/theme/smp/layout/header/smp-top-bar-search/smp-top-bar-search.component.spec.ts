import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpTopBarSearchComponent } from './smp-top-bar-search.component';

describe('SmpTopBarSearchComponent', () => {
  let component: SmpTopBarSearchComponent;
  let fixture: ComponentFixture<SmpTopBarSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpTopBarSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpTopBarSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
