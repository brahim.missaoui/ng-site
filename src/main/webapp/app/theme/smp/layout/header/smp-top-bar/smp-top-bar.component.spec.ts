import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpTopBarComponent } from './smp-top-bar.component';

describe('SmpTopBarComponent', () => {
  let component: SmpTopBarComponent;
  let fixture: ComponentFixture<SmpTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
