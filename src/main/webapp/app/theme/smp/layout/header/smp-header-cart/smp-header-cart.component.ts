import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../../../shared/ks/cart/cart.service";
import { cart } from "../../../../../shared/ks/cart/cart.model";

@Component({
  selector: 'jhi-app-smp-header-cart',
  templateUrl: './smp-header-cart.component.html',
  styleUrls: ['./smp-header-cart.component.css']
})
export class SmpHeaderCartComponent implements OnInit {
  public cart: cart.Cart;
  constructor(private cartService: CartService ) { }

  ngOnInit() {
    this.cartService.loadCart();
    this.cartService.currentCart$.subscribe((data) => this.cart = data );
  }

}
