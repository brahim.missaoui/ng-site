import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpTopBarCustomerLinksComponent } from './smp-top-bar-customer-links.component';

describe('SmpTopBarCustomerLinksComponent', () => {
  let component: SmpTopBarCustomerLinksComponent;
  let fixture: ComponentFixture<SmpTopBarCustomerLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpTopBarCustomerLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpTopBarCustomerLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
