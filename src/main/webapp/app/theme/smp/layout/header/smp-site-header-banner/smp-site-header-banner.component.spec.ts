import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpSiteHeaderBannerComponent } from './smp-site-header-banner.component';

describe('SmpSiteHeaderBannerComponent', () => {
  let component: SmpSiteHeaderBannerComponent;
  let fixture: ComponentFixture<SmpSiteHeaderBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpSiteHeaderBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpSiteHeaderBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
