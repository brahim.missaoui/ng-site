import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpSectionHeaderComponent } from './smp-section-header.component';

describe('SmpSectionHeaderComponent', () => {
  let component: SmpSectionHeaderComponent;
  let fixture: ComponentFixture<SmpSectionHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpSectionHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpSectionHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
