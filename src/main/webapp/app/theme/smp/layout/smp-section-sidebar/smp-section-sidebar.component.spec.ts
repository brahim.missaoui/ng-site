import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpSectionSidebarComponent } from './smp-section-sidebar.component';

describe('SmpSectionSidebarComponent', () => {
  let component: SmpSectionSidebarComponent;
  let fixture: ComponentFixture<SmpSectionSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpSectionSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpSectionSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
