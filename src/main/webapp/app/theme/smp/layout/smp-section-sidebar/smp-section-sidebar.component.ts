import { Component, OnInit } from '@angular/core';
import { catalog } from '../../../../shared/ks/catalog/catalog.model';
// import { CategoriesWrapper } from '../../../../shared/ks/catalog/categories-wrapper.model';
import { CatalogService } from '../../../../shared/ks/catalog/catalog.service';
import { Menu } from "../../../../shared/ks/menu/menu.model";
import { MenuService } from "../../../../shared/ks/menu/menu.service";

@Component({
  selector: 'jhi-app-smp-section-sidebar',
  templateUrl: './smp-section-sidebar.component.html',
  styleUrls: ['./smp-section-sidebar.component.css']
})
export class SmpSectionSidebarComponent implements OnInit {
    public categories: catalog.Category[];
    public menus: Menu[];

  constructor(private catalogService: CatalogService, private menuService: MenuService) { }

  ngOnInit() {
        this.menuService.listMenu('SideNav').subscribe((menus) => this.menus = menus);
        this.catalogService.categoryList().subscribe((categories) => this.categories = categories);

  }

  public reloadByRoute(catName: string) {
    this.catalogService.switchCategory(catName);
  }

  public clickMenuItem(menuElement: HTMLElement) {
    if(menuElement.classList.contains("open")){
      menuElement.click();
    }
  }


}
