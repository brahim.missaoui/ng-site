import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpWrapperComponent } from './smp-wrapper.component';

describe('SmpWrapperComponent', () => {
  let component: SmpWrapperComponent;
  let fixture: ComponentFixture<SmpWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
