import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpPaymentIconsComponent } from './smp-payment-icons.component';

describe('SmpPaymentIconsComponent', () => {
  let component: SmpPaymentIconsComponent;
  let fixture: ComponentFixture<SmpPaymentIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpPaymentIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpPaymentIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
