import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpFooterLinksComponent } from './smp-footer-links.component';

describe('SmpFooterLinksComponent', () => {
  let component: SmpFooterLinksComponent;
  let fixture: ComponentFixture<SmpFooterLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpFooterLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpFooterLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
