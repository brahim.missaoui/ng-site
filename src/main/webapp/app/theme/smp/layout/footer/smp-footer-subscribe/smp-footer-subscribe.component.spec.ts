import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpFooterSubscribeComponent } from './smp-footer-subscribe.component';

describe('SmpFooterSubscribeComponent', () => {
  let component: SmpFooterSubscribeComponent;
  let fixture: ComponentFixture<SmpFooterSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpFooterSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpFooterSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
