import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../../../shared/ks/menu/menu.model';
import { MenuService } from '../../../../../shared/ks/menu/menu.service';

@Component({
  selector: 'jhi-app-smp-footer-links',
  templateUrl: './smp-footer-links.component.html',
  styleUrls: ['./smp-footer-links.component.css']
})
export class SmpFooterLinksComponent implements OnInit {
    menus: Menu[];

  constructor(private menuService: MenuService) {}

    ngOnInit() {
        this.menuService.listMenu('Footer Nav').subscribe((menus) => this.menus = menus);
    }

}
