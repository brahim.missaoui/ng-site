import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpSectionFooterComponent } from './smp-section-footer.component';

describe('SmpSectionFooterComponent', () => {
  let component: SmpSectionFooterComponent;
  let fixture: ComponentFixture<SmpSectionFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpSectionFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpSectionFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
