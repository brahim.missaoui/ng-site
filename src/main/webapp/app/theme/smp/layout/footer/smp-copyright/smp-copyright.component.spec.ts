import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpCopyrightComponent } from './smp-copyright.component';

describe('SmpCopyrightComponent', () => {
  let component: SmpCopyrightComponent;
  let fixture: ComponentFixture<SmpCopyrightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpCopyrightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpCopyrightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
