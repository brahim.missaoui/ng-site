import { Component, OnInit } from '@angular/core';
import { CartService } from "../../../../shared/ks/cart/cart.service";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { cart } from "../../../../shared/ks/cart/cart.model";

@Component({
  selector: 'jhi-app-smp-cart',
  templateUrl: './smp-cart.component.html',
  styleUrls: ['./smp-cart.component.css']
})
export class SmpCartComponent implements OnInit {
  public cart: cart.Cart;

  public cartForm: FormGroup;



  constructor(public fb: FormBuilder,private cartService: CartService, private route: ActivatedRoute) {
   }

  ngOnInit() {
    this.cartService.promiseCart().then((cart)=>this.loadCartSuccessCallBack(cart)).catch((err)=>this.loadCartFailCallBack(err));
  }

  private loadCartSuccessCallBack(cart: cart.Cart){
    this.cart = cart;
    let i = 0;
    this.cartForm = this.fb.group({});
    cart.orderItems.forEach(orderItem => {
        let controlId = ''+orderItem.id;
        this.cartForm.addControl(controlId,new FormControl(orderItem.quantity, Validators.required));
    });
    this.cartService.loadCart();
  }

  private loadCartFailCallBack(err){
    console.log('loadCartFailCallBack' + err);
  }

  public removeOrderItem(orderItem: cart.OrderItem){
    this.cartService.removeOrderItem(this.cart, orderItem).then((cart)=> this.loadCartSuccessCallBack(cart)).catch((err)=>this.loadCartFailCallBack(err));
  }

  public updateOrderItemQuantity(cartId: number, orderItemId: number, quantity: number){
      this.cartService.updateOrderItemQuantity(this.cart.id, orderItemId, quantity).then((cart)=> this.loadCartSuccessCallBack(cart)).catch((err)=>this.loadCartFailCallBack(err));
  }

  public removeOrderItemById(orderItemId: number){
    this.cart.orderItems.forEach(orderItem => {
      if(orderItem.id == orderItemId){
        this.removeOrderItem(orderItem);
        return;
      }
    });
    
  }

  onCartFromSubmit(): void {
    Object.keys(this.cartForm.controls).forEach(key => {
      let control = this.cartForm.get(key);
      if(control.dirty){
        let quantity = Number.parseInt(control.value);
        let orderItemId = Number.parseInt(key);
        if(quantity < 1 ){
          this.removeOrderItemById(orderItemId);
        }else{
          this.updateOrderItemQuantity(this.cart.id, orderItemId, quantity);
        }
      }
    });
  }

}
