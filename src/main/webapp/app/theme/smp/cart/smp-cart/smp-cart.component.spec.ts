import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmpCartComponent } from './smp-cart.component';

describe('SmpCartComponent', () => {
  let component: SmpCartComponent;
  let fixture: ComponentFixture<SmpCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmpCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmpCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
