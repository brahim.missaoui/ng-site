import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { KapsulstoreSharedModule, UserRouteAccessService } from './shared';
 // import { KapsulstoreHomeModule } from './home/home.module';
 // import { KapsulstoreAdminModule } from './admin/admin.module';
 // import { KapsulstoreAccountModule } from './account/account.module';
import { KapsulstoreEntityModule } from './entities/entity.module';

import { LayoutRoutingModule } from './layouts';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

import { SmpWrapperComponent } from './theme/smp/layout/smp-wrapper/smp-wrapper.component';
import { SmpSectionHeaderComponent } from './theme/smp/layout/header/smp-section-header/smp-section-header.component';
import { SmpTopBarComponent } from './theme/smp/layout/header/smp-top-bar/smp-top-bar.component';
import { SmpTopBarSearchComponent } from './theme/smp/layout/header/smp-top-bar-search/smp-top-bar-search.component';
import { SmpTopBarCustomerLinksComponent } from './theme/smp/layout/header/smp-top-bar-customer-links/smp-top-bar-customer-links.component';
import { SmpHeaderCartComponent } from './theme/smp/layout/header/smp-header-cart/smp-header-cart.component';
import { SmpSiteHeaderBannerComponent } from './theme/smp/layout/header/smp-site-header-banner/smp-site-header-banner.component';
import { SmpSectionFooterComponent } from './theme/smp/layout/footer/smp-section-footer/smp-section-footer.component';
import { SmpCopyrightComponent } from './theme/smp/layout/footer/smp-copyright/smp-copyright.component';
import { SmpPaymentIconsComponent } from './theme/smp/layout/footer/smp-payment-icons/smp-payment-icons.component';
import { SmpFooterSubscribeComponent } from './theme/smp/layout/footer/smp-footer-subscribe/smp-footer-subscribe.component';
import { SmpFooterLinksComponent } from './theme/smp/layout/footer/smp-footer-links/smp-footer-links.component';
import { SmpSectionSidebarComponent } from './theme/smp/layout/smp-section-sidebar/smp-section-sidebar.component';
import { SmpMainContentComponent } from './theme/smp/layout/smp-main-content/smp-main-content.component';
import { SmpBreadcrumbComponent } from './theme/smp/shared/smp-breadcrumb/smp-breadcrumb.component';
import { SmpCustomerLoginComponent } from './theme/smp/account/login/smp-customer-login/smp-customer-login.component';
import { SmpHomeComponent } from './theme/smp/home/smp-home/smp-home.component';
import { Router, RouterLink, Routes, RouterModule } from '@angular/router';
import { SmpNewsComponent } from './theme/smp/blogs/smp-news/smp-news.component';
import { SmpContactUsComponent } from './theme/smp/pages/smp-contact-us/smp-contact-us.component';
import { SmpCustomerRegisterComponent } from './theme/smp/account/register/smp-customer-register/smp-customer-register.component';
import { SmpCartComponent } from './theme/smp/cart/smp-cart/smp-cart.component';

import { MenuService } from './shared/ks/menu/menu.service';
import { CatalogService } from './shared/ks/catalog/catalog.service';
import { SmpProductListComponent } from './theme/smp/product/smp-product-list/smp-product-list.component';
import { SmpFeaturedProductsComponent } from './theme/smp/product/smp-featured-products/smp-featured-products.component';
import { SmpCategoryProductsComponent } from './theme/smp/product/smp-category-products/smp-category-products.component';
import { SmpProductComponent } from './theme/smp/product/smp-product/smp-product.component';
import { CartService } from "./shared/ks/cart/cart.service";
import { CheckoutService } from "./shared/ks/checkout/checkout.service";
import { ReactiveFormsModule } from "@angular/forms";
import { NedenOrganikComponent } from './theme/smp/blogs/neden-organik/neden-organik';
import { SmpNewsInstagramComponent } from './theme/smp/blogs/smp-news-instagram/smp-news-instagram.component';
import { SmpAboutUsComponent } from './theme/smp/pages/smp-about-us/smp-about-us.component';
import { TermOfUseComponent } from './theme/smp/pages/term-of-use/term-of-use.component';
import { PrivacyAgreementComponent } from './theme/smp/pages/privacy-agreement/privacy-agreement.component';
import {ImageZoomModule} from 'angular2-image-zoom';
import { DeliveryAndReturnComponent } from './theme/smp/pages/delivery-and-return/delivery-and-return.component';
import { CheckoutComponent } from './theme/smp/checkout/component/checkout.component';
import { CheckoutSidebarComponent } from './theme/smp/checkout/checkout-sidebar/checkout-sidebar.component';
import { CheckoutMainComponent } from './theme/smp/checkout/checkout-main/checkout-main.component';
import { PaymentThanksComponent } from './theme/smp/pages/payment-thanks/payment-thanks.component';
import { PaymentErrorComponent } from './theme/smp/pages/payment-error/payment-error.component';
import { OrganicCertificateComponent } from './theme/smp/pages/organic-certificate/organic-certificate.component';
import { KapsulGardiropNedirComponent } from './theme/smp/blogs/kapsul-gardirop-nedir/kapsul-gardirop-nedir.component';

const appRoutes: Routes = [
{
path: 'catalog/:catName',
component: SmpCategoryProductsComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'product',
component: SmpProductComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'main',
component: SmpCategoryProductsComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'blogs/news',
component: SmpNewsComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'blogs/instagram',
component: SmpNewsInstagramComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/privacy',
component: PrivacyAgreementComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/delivery-and-return',
component: DeliveryAndReturnComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/term-of-usage',
component: TermOfUseComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'blogs/neden-organik',
component: NedenOrganikComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'blogs/kapsul-gardirop-nedir',
component: KapsulGardiropNedirComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/contact-us',
component: SmpContactUsComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/about-us',
component: SmpAboutUsComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/organic-certificate',
component: OrganicCertificateComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/payment-thanks',
component: PaymentThanksComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'pages/payment-error',
component: PaymentErrorComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'account/register',
component: SmpCustomerRegisterComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'account-login',
component: SmpCustomerLoginComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'cart',
component: SmpCartComponent,
data: {pageTitle: 'global.title'}
},
{
path: 'checkout',
component: CheckoutComponent,
data: {pageTitle: 'global.title'}
},
{ path: '',
  redirectTo: 'catalog/products',
  pathMatch: 'full',
  data: {pageTitle: 'global.title'}
  // , component: SmpCategoryProductsComponent
},
{ path: '**', component: SmpProductComponent,
  data: {pageTitle: 'global.title'}
 }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {useHash: true} ),
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        KapsulstoreSharedModule,
        // KapsulstoreHomeModule,
        // KapsulstoreAdminModule,
        // KapsulstoreAccountModule,
        KapsulstoreEntityModule,
        ReactiveFormsModule,
        ImageZoomModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
SmpWrapperComponent,
    SmpSectionHeaderComponent,
    SmpTopBarComponent,
    SmpTopBarSearchComponent,
    SmpTopBarCustomerLinksComponent,
    SmpHeaderCartComponent,
    SmpSiteHeaderBannerComponent,
    SmpSectionFooterComponent,
    SmpCopyrightComponent,
    SmpPaymentIconsComponent,
    SmpFooterSubscribeComponent,
    SmpFooterLinksComponent,
    SmpSectionSidebarComponent,
    SmpMainContentComponent,
    SmpBreadcrumbComponent,
    SmpCustomerLoginComponent,
    SmpHomeComponent,
    SmpNewsComponent,
    SmpContactUsComponent,
    SmpCustomerRegisterComponent,
    SmpCartComponent,
    SmpProductListComponent,
    SmpFeaturedProductsComponent,
    SmpCategoryProductsComponent,
    SmpProductComponent,
    NedenOrganikComponent,
    SmpNewsInstagramComponent,
    SmpAboutUsComponent,
    TermOfUseComponent,
    PrivacyAgreementComponent,
    DeliveryAndReturnComponent,
    CheckoutComponent,
    CheckoutSidebarComponent,
    CheckoutMainComponent,
    PaymentThanksComponent,
    PaymentErrorComponent,
    OrganicCertificateComponent,
    KapsulGardiropNedirComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        MenuService,
        CatalogService,
        CartService,
        CheckoutService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class KapsulstoreAppModule {}
